unit MAWFormNotarizeInfo;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TFormMacNotarizeInfo }

  TFormMacNotarizeInfo = class(TForm)
    labelRequestUUID: TLabel;
    labelDate: TLabel;
    labelLogFileURL: TLabel;
    labelStatusMessage: TLabel;
    labelStatusCode: TLabel;
    labelStatus: TLabel;
    memoLog: TMemo;
  private
    FAppleID: string;
    FKeyChainEntryName: string;
    FRequestUUID: string;
    procedure RefreshInfo;
  public
    function Execute(AAppleID, AKeyChainEntryName, ARequestUUID: string): boolean;
  end;

function ShowNotarizeInfo(AAppleID, AKeyChainEntryName, ARequestUUID: string): boolean;

implementation

uses
  MAWHandling;

function ShowNotarizeInfo(AAppleID, AKeyChainEntryName, ARequestUUID: string): boolean;
var
  form: TFormMacNotarizeInfo;
begin
  form := TFormMacNotarizeInfo.Create(Application.MainForm);
  try
    Result := form.Execute(AAppleID, AKeyChainEntryName, ARequestUUID);
  finally
    form.Free;
  end;
end;

{$R *.lfm}

{ TFormMacNotarizeInfo }

procedure TFormMacNotarizeInfo.RefreshInfo;
var
  sl: TStringList;
  i: integer;
begin
  sl := TStringList.Create;
  try
    sl.NameValueSeparator := ':';
    MAWExecuteProcess('xcrun', Format('altool --notarization-info %s -u "%s" -p "@keychain:%s"', [FRequestUUID, FAppleID, FKeyChainEntryName]), sl, [0]);
    for i := 0 to Pred(sl.Count) do begin
      sl[i] := Trim(sl[i]);
    end;
    labelRequestUUID.Caption := 'RequestUUID: ' + sl.Values['RequestUUID'];
    labelDate.Caption := 'Date: ' + sl.Values['Date'];
    labelStatus.Caption := 'Status: ' + sl.Values['Status'];
    labelStatusCode.Caption := 'Status Code: ' + sl.Values['Status Code'];
    labelStatusMessage.Caption := 'Status Message: ' + sl.Values['Status Message'];
    labelLogFileURL.Caption := 'Log File URL: ' + sl.Values['LogFileURL'];
    memoLog.Lines.Clear;
    memoLog.Lines.Add('Log file not yet loaded.');
    memoLog.Lines.Add(sl.Values['LogFileURL']);
  finally
    sl.Free;
  end;

end;

function TFormMacNotarizeInfo.Execute(AAppleID, AKeyChainEntryName, ARequestUUID: string): boolean;
begin
  FAppleID := AAppleID;
  FKeyChainEntryName := AKeyChainEntryName;
  FRequestUUID := ARequestUUID;
  RefreshInfo;
  Result := ShowModal = mrOk;
end;

end.



