unit MAWLazarusProjectFile;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  laz2_DOM,
  laz2_xmlread,
  laz2_xmlwrite,
  laz2_xpath;

type

  { TLazarusProjectFile }

  TLazarusProjectFile = class
  private
    FFilename: string;
    FDocument: TXMLDocument;
    FVersionInfoFields: TStringList;
    procedure FillVersionInfoFields;
    function GetDpiAware: boolean;
    function GetUniformTypeIdentifier: string;
    function GetVersionProductName: string;
    function GetVersionProductVersion: string;
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadFromFile(AFilename: string);
    procedure SaveToFile;
    property VersionInfoFields: TStringList read FVersionInfoFields;
    property VersionProductName: string read GetVersionProductName;
    property VersionProductVersion: string read GetVersionProductVersion;
    property UniformTypeIdentifier: string read GetUniformTypeIdentifier;
    property DpiAware: boolean read GetDpiAware;
  end;

implementation

{ TLazarusProjectFile }

procedure TLazarusProjectFile.FillVersionInfoFields;
var
  r: TXPathVariable;
  n: TDOMElement;
  i: integer;
  sKey: string;
begin
  FVersionInfoFields.Clear;
  if not Assigned(FDocument) then begin
    Exit;
  end;
  r := EvaluateXPathExpression('/CONFIG/ProjectOptions/VersionInfo/StringTable', FDocument.DocumentElement);
  try
     if Assigned(r.AsNodeSet) then begin
        if r.AsNodeSet.Count = 1 then begin
           n := TDOMElement(r.AsNodeSet[0]);
           for i := 0 to Pred(n.Attributes.Length) do begin
              sKey := n.Attributes[i].NodeName;
              FVersionInfoFields.Values[sKey] := n.GetAttribute(sKey);
           end;
        end;
     end;
  finally
    r.Free;
  end;
end;

function TLazarusProjectFile.GetDpiAware: boolean;
var
  r: TXPathVariable;
  n: TDOMElement;
  i: integer;
  sKey: string;
begin
  Result := false;
  if not Assigned(FDocument) then begin
    Exit;
  end;
  r := EvaluateXPathExpression('/CONFIG/ProjectOptions/General/XPManifest/DpiAware', FDocument.DocumentElement);
  try
     if Assigned(r.AsNodeSet) then begin
        if r.AsNodeSet.Count = 1 then begin
           n := TDOMElement(r.AsNodeSet[0]);
           Result := SameText(n.GetAttribute('Value'), 'true');
        end;
     end;
  finally
    r.Free;
  end;
end;

function TLazarusProjectFile.GetUniformTypeIdentifier: string;
var
  r: TXPathVariable;
  n: TDOMElement;
  i: integer;
  sKey: string;
begin
  Result := '';
  if not Assigned(FDocument) then begin
    Exit;
  end;
  r := EvaluateXPathExpression('/CONFIG/ProjectOptions/General/XPManifest/TextName', FDocument.DocumentElement);
  try
     if Assigned(r.AsNodeSet) then begin
        if r.AsNodeSet.Count = 1 then begin
           n := TDOMElement(r.AsNodeSet[0]);
           Result := n.GetAttribute('Value');
        end;
     end;
  finally
    r.Free;
  end;
end;

function TLazarusProjectFile.GetVersionProductName: string;
begin
  Result := FVersionInfoFields.Values['ProductName'];
end;

function TLazarusProjectFile.GetVersionProductVersion: string;
begin
  Result := FVersionInfoFields.Values['ProductVersion'];
end;

constructor TLazarusProjectFile.Create;
begin
  FDocument := nil;
  FVersionInfoFields := TStringList.Create;
end;

destructor TLazarusProjectFile.Destroy;
begin
  FDocument.Free;
  FVersionInfoFields.Free;
  inherited Destroy;
end;

procedure TLazarusProjectFile.LoadFromFile(AFilename: string);
begin
  FVersionInfoFields.Clear;
  FDocument.Free;
  FFilename := AFilename;
  ReadXMLFile(FDocument, AFilename);
  FillVersionInfoFields;
end;

procedure TLazarusProjectFile.SaveToFile;
begin
  WriteXMLFile(FDocument, FFilename);
end;

end.
(*
<?xml version="1.0" encoding="UTF-8"?>
<CONFIG>
  <ProjectOptions>
    <Version Value="11"/>
    <PathDelim Value="\"/>
    <General>
      <SessionStorage Value="InProjectDir"/>
      <MainUnit Value="0"/>
      <Title Value="Spybot Identity Monitor"/>
      <Scaled Value="True"/>
      <ResourceType Value="res"/>
      <UseXPManifest Value="True"/>
      <XPManifest>
        <DpiAware Value="True"/>
        <TextName Value="SaferNetworking.Spybot3.IdentityMonitor"/>
        <TextDesc Value="Monitors your email addresses for leaks."/>
      </XPManifest>
      <Icon Value="0"/>
      <Resources Count="8">
        <Resource_0 FileName="..\..\bin\IdentityMonitor-Release\fonts\WorkSans-ExtraLight.ttf" Type="RCDATA" ResourceName="WORKSANS-EXTRALIGHT"/>
        <Resource_1 FileName="..\..\bin\IdentityMonitor-Release\fonts\WorkSans-Regular.ttf" Type="RCDATA" ResourceName="WORKSANS-REGULAR"/>
        <Resource_2 FileName="..\..\bin\IdentityMonitor-Release\fonts\WorkSans-SemiBold.ttf" Type="RCDATA" ResourceName="WORKSANS-SEMIBOLD"/>
        <Resource_3 FileName="..\..\bin\IdentityMonitor-Release\fonts\WorkSans-Thin.ttf" Type="RCDATA" ResourceName="WORKSANS-THIN"/>
        <Resource_4 FileName="nextsteps.html" Type="RCDATA" ResourceName="NEXTSTEPS"/>
        <Resource_5 FileName="Spybot3IdentityMonitor.html" Type="RCDATA" ResourceName="MANUALHTML"/>
        <Resource_6 FileName="charta-en.html" Type="RCDATA" ResourceName="CHARTA-EN"/>
        <Resource_7 FileName="privacypolicy.html" Type="RCDATA" ResourceName="PRIVACYPOLICY"/>
      </Resources>
    </General>
    <i18n>
      <EnableI18N Value="True"/>
      <OutDir Value="..\..\bin\IdentityMonitor-Debug\locale"/>
    </i18n>
    <VersionInfo>
      <UseVersionInfo Value="True"/>
      <AutoIncrementBuild Value="True"/>
      <MajorVersionNr Value="3"/>
      <MinorVersionNr Value="1"/>
      <RevisionNr Value="2"/>
      <BuildNr Value="9"/>
      <StringTable Comments="Verifies if your personal data has been breached by web services." CompanyName="Safer-Networking Ltd." FileDescription="Spybot Identity Monitor" LegalCopyright="Â© 2018 Safer-Networking Ltd. All rights reserved." LegalTrademarks="Spybot is a registered trademark by Patrick Kolla-ten Venne" OriginalFilename="Spybot3IdentityMonitor.exe" ProductName="Spybot Identity Monitor" ProductVersion="3.1"/>
    </VersionInfo>
*)
