program LazarusMacAppWizard;

{$mode objfpc}{$H+}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads, {$ENDIF} {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  MAWFormMain,
  MAWFormEntitlements,
  MAWInfoPList,
  MAWLazarusProjectFile,
  MAWFormNotarize, MAWFormNotarizeInfo;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Title:='Lazarus Mac App Wizard';
  Application.Scaled := True;
  Application.Initialize;
  Application.CreateForm(TFormLazarusMacApplicationWizard, 
    FormLazarusMacApplicationWizard);
  Application.Run;
end.
