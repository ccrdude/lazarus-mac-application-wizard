unit MAWFormMain;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Forms,
  Controls,
  Graphics,
  Dialogs,
  StdCtrls,
  ExtCtrls,
  EditBtn,
  ComCtrls,
  ActnList,
  IniFiles,
  Generics.Collections,
  MAWFormEntitlements,
  MAWFormInfo,
  MAWFormNotarize,
  MAWHandling;

type

  { TFormLazarusMacApplicationWizard }

  TFormLazarusMacApplicationWizard = class(TForm)
    aCopyExecutableIntoAppBundle: TAction;
    aCreateKeyChainEntry: TAction;
    aShowNotarizeResults: TAction;
    aEditAppBundleInfo: TAction;
    aEditEntitlements: TAction;
    aProcess: TAction;
    aTestEntitlements: TAction;
    aSignAppBundle: TAction;
    alWizard: TActionList;
    bnEditAppBundleInfo: TButton;
    bnShowNotarizeResults: TButton;
    bnCreateKeyChainEntry: TButton;
    bnProcess: TButton;
    bnEditEntitlements: TButton;
    cbNonDeep: TCheckBox;
    cbNotarize: TCheckBox;
    cbDistribution: TComboBox;
    editAppleID: TComboBox;
    editSigningIdentityAppBundle: TComboBox;
    editKeyChainEntryName: TEditButton;
    editInputInstallerPath: TFileNameEdit;
    editInputProjectFile: TFileNameEdit;
    editInputProjectBinary: TFileNameEdit;
    editInputApplicationBundle: TFileNameEdit;
    editInputEntitlementsFile: TFileNameEdit;
    editSigningIdentityAppBundleDevID: TComboBox;
    editSigningIdentityInstaller: TComboBox;
    gbDeveloperOutside: TGroupBox;
    gbProjectInput: TGroupBox;
    gbDeveloperAppStore: TGroupBox;
    groupAppleID: TGroupBox;
    labelKeyChainEntryNameHelp: TLabel;
    labelAppleID: TLabel;
    labelKeyChainEntryName: TLabel;
    labelSigningIdentityAppBundle1: TLabel;
    labelSigningIdentityInstaller: TLabel;
    labelStatus: TLabel;
    labelInputEntitlementsFile: TLabel;
    labelSigningIdentityAppBundle: TLabel;
    labelInputProjectFile: TLabel;
    labelInputProjectBinary: TLabel;
    labelInputApplicationBundle: TLabel;
    labelInputInstallerPath: TLabel;
    memoLog: TMemo;
    pcWizard: TPageControl;
    pg: TProgressBar;
    tabProject: TTabSheet;
    tabLog: TTabSheet;
    tabCertificates: TTabSheet;
    tabAppleID: TTabSheet;
    procedure aCopyExecutableIntoAppBundleExecute(Sender: TObject);
    procedure aCreateKeyChainEntryExecute(Sender: TObject);
    procedure aEditAppBundleInfoExecute(Sender: TObject);
    procedure aEditEntitlementsExecute(Sender: TObject);
    procedure aProcessExecute(Sender: TObject);
    procedure aShowNotarizeResultsExecute(Sender: TObject);
    procedure aSignAppBundleExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FWizard: TLazarusMacAppWizard;
    {
      UpdateWizard copies the UI settings into the wizard class.
    }
    procedure UpdateWizard;
    {
      This retrieves all existing codesigning certficates using a command line query.

      @see(https://stackoverflow.com/questions/7747230/determining-codesigning-identities-from-the-command-line)

      It executes and evaluates the output of:
      security find-identity -v
    }
    procedure FillDeveloperIDs;
    {
      Tries to guess your develope Apple ID by determining the Apple ID
      associated with the current user account.

      It executes and evaluates the output of:
      dscl . readpl /Users/<username> dsAttrTypeNative:LinkedIdentity appleid.apple.com:linked\ identities:0:full\ name
    }
    procedure FillAppleID;
    {
      LoadDefaults retrieves default values form a defaults.ini file.
      This should be replaced with a future UI value persistence.
    }
    procedure LoadDeveloperSettings;
    procedure SaveDeveloperSettings;
    procedure LoadProjectSettings;
    procedure SaveProjectSettings;
  public
  end;

var
  FormLazarusMacApplicationWizard: TFormLazarusMacApplicationWizard;

implementation

uses
  RegExpr,
  Process;

resourcestring
  rsDialogKeychainEntryCaption = 'Create a keychain entry for your Apple ID';
  rsDialogKeychainEntryPrompt1 = 'Please specify your Apple ID password to create a keychain entry.';
  rsDialogKeychainEntryPrompt2 = 'This allows you to use this wizard without giving it your password in the future.';
  rsDialogKeychainEntryPrompt3 = 'If you have enabled Two Factor Authentication, please create an app-specific one on appleid.apple.com.';
  rsFinished = 'Finished.';

{$R *.lfm}

{ TFormLazarusMacApplicationWizard }

procedure TFormLazarusMacApplicationWizard.FormDestroy(Sender: TObject);
begin
  FWizard.Free;
end;

procedure TFormLazarusMacApplicationWizard.FormShow(Sender: TObject);
begin
  LoadDeveloperSettings;
  LoadProjectSettings;
  FillDeveloperIDs;
  FillAppleID;
end;

procedure TFormLazarusMacApplicationWizard.UpdateWizard;
begin
  FWizard.VerbosityLevel := 3;
  FWizard.LazarusProjectFile := editInputProjectFile.Text;
  FWizard.BinaryPath := editInputProjectBinary.Text;
  FWizard.EntitlementsPath := editInputEntitlementsFile.Text;
  FWizard.AppBundlePath := editInputApplicationBundle.Text;
  FWizard.InstallerPath := editInputInstallerPath.Text;
  FWizard.SigningIdentityThirdPartyMacDevAppBundle := editSigningIdentityAppBundle.Text;
  FWizard.SigningIdentityThirdPartyMacDevInstaller := editSigningIdentityInstaller.Text;
  FWizard.SigningIdentityDeveloperIDAppBundle := editSigningIdentityAppBundleDevID.Text;
  FWizard.AppleID := editAppleID.Text;
  FWizard.KeychainEntryName := editKeyChainEntryName.Text;
  if cbNonDeep.Checked then begin
    FWizard.Options := FWizard.Options + [mabwoNoDeepSign];
  end else begin
    FWizard.Options := FWizard.Options - [mabwoNoDeepSign];
  end;
  if cbNotarize.Checked then begin
    FWizard.Options := FWizard.Options + [mabwoNotarize];
  end else begin
    FWizard.Options := FWizard.Options - [mabwoNotarize];
  end;
  case cbDistribution.ItemIndex of
    0: // App Store
    begin
      FWizard.Options := FWizard.Options + [mabwoAppStore];
    end;
    1: // outside
    begin
      FWizard.Options := FWizard.Options - [mabwoAppStore];
    end;
  end;
end;

procedure TFormLazarusMacApplicationWizard.FillDeveloperIDs;
var
  sl: TStringList;
  r: TRegExpr;
  i: integer;
begin
  editSigningIdentityAppBundle.Items.BeginUpdate;
  editSigningIdentityInstaller.Items.BeginUpdate;
  editSigningIdentityAppBundleDevID.Items.BeginUpdate;
  try
    editSigningIdentityAppBundle.Items.Clear;
    editSigningIdentityAppBundle.Items.Add('3rd Party Mac Developer Application');
    editSigningIdentityInstaller.Items.Clear;
    editSigningIdentityInstaller.Items.Add('3rd Party Mac Developer Installer');
    editSigningIdentityAppBundleDevID.Items.Clear;
    editSigningIdentityAppBundleDevID.Items.Add('Developer ID Application');
    sl := TStringList.Create;
    try
      MAWExecuteProcess('security', 'find-identity -v', sl, [0]);
    (*
      5) 1234567890123456789012345678901234567890 "Developer ID Application: COMPANYNAME (CAFFEE007)"
      6) 1234567890123456789012345678901234567890 "Mac Developer: APPLEID (CAFFEE007)"
     *)
      r := TRegExpr.Create('\s*[0-9]*\)\s+([0-9A-Z]*) "([^\:]*:\s*([^\(]*\(([0-9A-Z-]*)\)))"');
      try
        for i := 0 to Pred(sl.Count) do begin
          if r.Exec(sl[i]) then begin
            editSigningIdentityAppBundle.Items.Add(r.Match[2]);
            editSigningIdentityInstaller.Items.Add(r.Match[2]);
            editSigningIdentityAppBundleDevID.Items.Add(r.Match[2]);
          end;
        end;
      finally
        r.Free;
      end;
    finally
      sl.Free;
    end;
  finally
    editSigningIdentityAppBundle.Items.EndUpdate;
    editSigningIdentityInstaller.Items.EndUpdate;
    editSigningIdentityAppBundleDevID.Items.EndUpdate;
  end;
end;

procedure TFormLazarusMacApplicationWizard.FillAppleID;
var
  sl: TStringList;
  r: TRegExpr;
  i: integer;
  sParameters: string;
begin
  editAppleID.Items.BeginUpdate;
  try
    editAppleID.Items.Clear;
    sl := TStringList.Create;
    try
      sParameters := '.|readpl|/Users/' + GetEnvironmentVariable('USER') + '|dsAttrTypeNative:LinkedIdentity|appleid.apple.com:linked identities:0:full name';
      MAWExecuteProcess('dscl', sParameters, sl, [0], '|');
      (*
      appleid.apple.com:linked identities:0:full name: XXXXXXXXX
      *)
      r := TRegExpr.Create('appleid\.apple\.com:linked identities:0:full name: ([^\$]*)');
      try
        for i := 0 to Pred(sl.Count) do begin
          if r.Exec(sl[i]) then begin
            editAppleID.Items.Add(r.Match[1]);
          end;
        end;
      finally
        r.Free;
      end;
    finally
      sl.Free;
    end;
  finally
    editAppleID.Items.EndUpdate;
  end;
end;

procedure TFormLazarusMacApplicationWizard.LoadDeveloperSettings;
var
  sFilename: string;
  mif: TMemIniFile;
begin
  sFilename := GetAppConfigDir(False) + 'developer.ini';
  mif := TMemIniFile.Create(sFilename);
  try
    editSigningIdentityAppBundle.Text := mif.ReadString('developer', 'SigningIdentityAppBundle', editSigningIdentityAppBundle.Text);
    editSigningIdentityInstaller.Text := mif.ReadString('developer', 'SigningIdentityInstaller', editSigningIdentityInstaller.Text);
    editSigningIdentityAppBundleDevID.Text := mif.ReadString('developer', 'SigningIdentityAppBundleDevID', editSigningIdentityAppBundleDevID.Text);
    editAppleID.Text := mif.ReadString('developer', 'AppleID', editAppleID.Text);
    editKeyChainEntryName.Text := mif.ReadString('developer', 'KeyChainEntryName', editKeyChainEntryName.Text);
  finally
    mif.Free;
  end;
end;

procedure TFormLazarusMacApplicationWizard.SaveDeveloperSettings;
var
  sFilename: string;
  mif: TMemIniFile;
begin
  sFilename := GetAppConfigDir(False) + 'developer.ini';
  ForceDirectories(ExtractFilePath(sFilename));
  mif := TMemIniFile.Create(sFilename);
  try
    mif.WriteString('developer', 'SigningIdentityAppBundle', editSigningIdentityAppBundle.Text);
    mif.WriteString('developer', 'SigningIdentityInstaller', editSigningIdentityInstaller.Text);
    mif.WriteString('developer', 'SigningIdentityAppBundleDevID', editSigningIdentityAppBundleDevID.Text);
    mif.WriteString('developer', 'AppleID', editAppleID.Text);
    mif.WriteString('developer', 'KeyChainEntryName', editKeyChainEntryName.Text);
    mif.UpdateFile;
  finally
    mif.Free;
  end;
end;

procedure TFormLazarusMacApplicationWizard.LoadProjectSettings;
var
  sFilename: string;
  mif: TMemIniFile;
begin
  sFilename := GetAppConfigDir(False) + 'last_project.ini';
  mif := TMemIniFile.Create(sFilename);
  try
    editInputInstallerPath.Text := mif.ReadString('project', 'InstallerPath', editInputInstallerPath.Text);
    editInputProjectFile.Text := mif.ReadString('project', 'ProjectFile', editInputProjectFile.Text);
    editInputProjectBinary.Text := mif.ReadString('project', 'ProjectBinary', editInputProjectBinary.Text);
    editInputApplicationBundle.Text := mif.ReadString('project', 'ApplicationBundle', editInputApplicationBundle.Text);
    editInputEntitlementsFile.Text := mif.ReadString('project', 'EntitlementsFile', editInputEntitlementsFile.Text);
  finally
    mif.Free;
  end;
end;

procedure TFormLazarusMacApplicationWizard.SaveProjectSettings;
var
  sFilename: string;
  mif: TMemIniFile;
begin
  sFilename := GetAppConfigDir(False) + 'last_project.ini';
  ForceDirectories(ExtractFilePath(sFilename));
  mif := TMemIniFile.Create(sFilename);
  try
    mif.WriteString('project', 'InstallerPath', editInputInstallerPath.Text);
    mif.WriteString('project', 'ProjectFile', editInputProjectFile.Text);
    mif.WriteString('project', 'ProjectBinary', editInputProjectBinary.Text);
    mif.WriteString('project', 'ApplicationBundle', editInputApplicationBundle.Text);
    mif.WriteString('project', 'EntitlementsFile', editInputEntitlementsFile.Text);
    mif.UpdateFile;
  finally
    mif.Free;
  end;
end;

procedure TFormLazarusMacApplicationWizard.FormCreate(Sender: TObject);
begin
  FWizard := TLazarusMacAppWizard.Create;
end;

procedure TFormLazarusMacApplicationWizard.aCopyExecutableIntoAppBundleExecute
  (Sender: TObject);
begin
  UpdateWizard;
  FWizard.CopyExecutableIntoBundle;
  memoLog.Lines.AddStrings(FWizard.LastActionLog);
  tabLog.Show;
end;

procedure TFormLazarusMacApplicationWizard.aCreateKeyChainEntryExecute(Sender: TObject);
var
  sl: TStringList;
  sPassword: string = '';
begin
  sl := TStringList.Create;
  try
    if InputQuery(rsDialogKeychainEntryCaption, rsDialogKeychainEntryPrompt1 + #13#10 + rsDialogKeychainEntryPrompt2 + #13#10 + rsDialogKeychainEntryPrompt3, sPassword) then
    begin
      MAWExecuteProcess('security', Format('add-generic-password -a "%s" -w "%s" -s "%s"', [editAppleID.Text, sPassword, editKeyChainEntryName.Text]), sl, [0]);
      ShowMessage(sl.Text);
    end;
  finally
    sl.Free;
  end;
end;

procedure TFormLazarusMacApplicationWizard.aEditAppBundleInfoExecute(Sender: TObject);
begin
  EditAppBundleInfo(IncludeTrailingPathDelimiter(editInputApplicationBundle.Text) + 'Contents/Info.plist');
end;

procedure TFormLazarusMacApplicationWizard.aEditEntitlementsExecute(Sender: TObject);
begin
  EditEntitlements(editInputEntitlementsFile.Text);
end;

procedure TFormLazarusMacApplicationWizard.aProcessExecute(Sender: TObject);
type
  TEmptyMethod = function(): boolean of object;
  TEmptyMethodList = specialize TList<TEmptyMethod>;

  function TestMethod(AMethod: TEmptyMethod): boolean;
  begin
    Result := AMethod();
    memoLog.Lines.AddStrings(FWizard.LastActionLog);
    memoLog.Lines.Add('');
  end;

var
  pm: TEmptyMethodList;
  m: TEmptyMethod;
begin
  aProcess.Enabled := False;
  try
    UpdateWizard;
    pg.Position := 0;
    pm := TEmptyMethodList.Create;
    try
      pm.Add(@FWizard.ForgetPackageId);
      pm.Add(@FWizard.CopyExecutableIntoBundle);
      pm.Add(@FWizard.RemoveExtendedAttributes);
      pm.Add(@FWizard.RemoveMetaFiles);
      pm.Add(@FWizard.ChangePermissions);
      pm.Add(@FWizard.InfoPListBinaryToXML);
      pm.Add(@FWizard.TestInfoPList);
      pm.Add(@FWizard.TestIcon);
      pm.Add(@FWizard.TestEntitlements);
      pm.Add(@FWizard.SignApplicationBundle);
      pm.Add(@FWizard.TestGateKeeperAppBundle);
      pm.Add(@FWizard.CreateInstaller);
      pm.Add(@FWizard.SignInstaller);
      pm.Add(@FWizard.TestGateKeeperInstaller);
      pm.Add(@FWizard.NotarizeInstaller);

      pg.Max := (pm.Count);
      for m in pm do begin
        if not TestMethod(m) then begin
          tabLog.Show;
          Exit;
        end;
        if memoLog.Lines.Count > 2 then begin
          labelStatus.Caption := memoLog.Lines[memoLog.Lines.Count - 2];
          labelStatus.Invalidate;
        end;
        pg.Position := pg.Position + 1;
        pg.Invalidate;
        Application.ProcessMessages;
      end;
      labelStatus.Caption := rsFinished;
    finally
      pm.Free;
    end;
  finally
    aProcess.Enabled := True;
  end;
end;

procedure TFormLazarusMacApplicationWizard.aShowNotarizeResultsExecute(Sender: TObject);
begin
  ShowNotarize(editAppleID.Text, editKeyChainEntryName.Text);
end;


procedure TFormLazarusMacApplicationWizard.aSignAppBundleExecute(Sender: TObject);
begin
  UpdateWizard;
  FWizard.SignApplicationBundle;
  memoLog.Lines.AddStrings(FWizard.LastActionLog);
  tabLog.Show;
end;

procedure TFormLazarusMacApplicationWizard.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  SaveDeveloperSettings;
  SaveProjectSettings;
  CanClose := True;
end;

end.
