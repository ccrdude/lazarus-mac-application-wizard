unit MAWFormInfo;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Forms,
  Controls,
  Graphics,
  Dialogs,
  ButtonPanel,
  StdCtrls,
  ExtCtrls,
  MAWInfoPList;

type

  { TFormMacInfoEditor }

  TFormMacInfoEditor = class(TForm)
    buttonsDialog: TButtonPanel;
    edit_CFBundleName: TEdit;
    edit_CFBundleDisplayName: TEdit;
    label_CFBundleName: TLabel;
    label_CFBundleDisplayName: TLabel;
    ScrollBox1: TScrollBox;
  private
    FFilename: string;
    FInfo: TAppleInfoPList;
    procedure LoadData;
    procedure SaveData;
  public
    function Execute(AFilename: string): boolean;
  end;

function EditAppBundleInfo(AFilename: string): boolean;

implementation

function EditAppBundleInfo(AFilename: string): boolean;
var
  form: TFormMacInfoEditor;
begin
  form := TFormMacInfoEditor.Create(Application.MainForm);
  try
    Result := form.Execute(AFilename);
  finally
    form.Free;
  end;
end;

{$R *.lfm}

{ TFormMacInfoEditor }

procedure TFormMacInfoEditor.LoadData;
begin
  edit_CFBundleName.Text := FInfo.CFBundleName;
  edit_CFBundleDisplayName.Text := FInfo.CFBundleDisplayName;
end;

procedure TFormMacInfoEditor.SaveData;
begin
  FInfo.CFBundleName := edit_CFBundleName.Text;
  FInfo.CFBundleDisplayName := edit_CFBundleDisplayName.Text;
end;

function TFormMacInfoEditor.Execute(AFilename: string): boolean;
begin
  FFilename := AFilename;
  FInfo := TAppleInfoPList.Create;
  try
    FInfo.LoadFromFile(AFilename);
    LoadData;
    Result := (ShowModal = mrOk);
    if Result then begin
      SaveData;
      FInfo.SaveToFile;
    end;
  finally
    FInfo.Free;
  end;
end;

end.


