unit MAWFormNotarize;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Forms,
  Controls,
  Graphics,
  Dialogs,
  ComCtrls,
  StdCtrls,
  RegExpr;

type

  { TRequestListItem }

  TRequestListItem = class(TListItem)
  private
    procedure EnsureSubItemCount;
    function GetDate: string;
    function GetStatusCode: string;
    function GetStatusMessage: string;
    function GetStatusShort: string;
    function GetUUID: string;
    procedure SetDate(AValue: string);
    procedure SetStatusCode(AValue: string);
    procedure SetStatusMessage(AValue: string);
    procedure SetStatusShort(AValue: string);
    procedure SetUUID(AValue: string);
  public
    property RequestUUID: string read GetUUID write SetUUID;
    property StatusMessage: string read GetStatusMessage write SetStatusMessage;
    property StatusCode: string read GetStatusCode write SetStatusCode;
    property StatusShort: string read GetStatusShort write SetStatusShort;
    property Date: string read GetDate write SetDate;
  end;

  { TFormMacNotarizeResults }

  TFormMacNotarizeResults = class(TForm)
    bnRefresh: TButton;
    lvResults: TListView;
    procedure bnRefreshClick(Sender: TObject);
    procedure lvResultsCreateItemClass(Sender: TCustomListView; var ItemClass: TListItemClass);
    procedure lvResultsDblClick(Sender: TObject);
  private
    FAppleID: string;
    FKeyChainEntryName: string;
    {
      Fills the list with all current notarize jobs.

      It executes and evaluates the output of:
      xcrun altool --notarization-history 0 -u "<apple id>" -p "@keychain:<key chain entry namee>"
    }
    procedure RefreshItems;
  public
    function Execute(AAppleID, AKeyChainEntryName: string): boolean;
  end;

function ShowNotarize(AAppleID, AKeyChainEntryName: string): boolean;

implementation

uses
  MAWHandling,
  MAWFormNotarizeInfo;

function ShowNotarize(AAppleID, AKeyChainEntryName: string): boolean;
var
  form: TFormMacNotarizeResults;
begin
  form := TFormMacNotarizeResults.Create(Application.MainForm);
  try
    Result := form.Execute(AAppleID, AKeyChainEntryName);
  finally
    form.Free;
  end;
end;

{$R *.lfm}

{ TRequestListItem }

procedure TRequestListItem.EnsureSubItemCount;
begin
  while SubItems.Count < 4 do begin
    SubItems.Add('');
  end;
end;

function TRequestListItem.GetDate: string;
begin
  Result := Caption;
end;

function TRequestListItem.GetStatusCode: string;
begin
  EnsureSubItemCount;
  Result := SubItems[2];
end;

function TRequestListItem.GetStatusMessage: string;
begin
  EnsureSubItemCount;
  Result := SubItems[0];
end;

function TRequestListItem.GetStatusShort: string;
begin
  EnsureSubItemCount;
  Result := SubItems[3];
end;

function TRequestListItem.GetUUID: string;
begin
  EnsureSubItemCount;
  Result := SubItems[1];
end;

procedure TRequestListItem.SetDate(AValue: string);
begin
  Caption := AValue;
end;

procedure TRequestListItem.SetStatusCode(AValue: string);
begin
  EnsureSubItemCount;
  SubItems[2] := AValue;
end;

procedure TRequestListItem.SetStatusMessage(AValue: string);
begin
  EnsureSubItemCount;
  SubItems[0] := AValue;
end;

procedure TRequestListItem.SetStatusShort(AValue: string);
begin
  EnsureSubItemCount;
  SubItems[3] := AValue;
end;

procedure TRequestListItem.SetUUID(AValue: string);
begin
  EnsureSubItemCount;
  SubItems[1] := AValue;
end;

{ TFormMacNotarizeResults }

procedure TFormMacNotarizeResults.bnRefreshClick(Sender: TObject);
begin
  RefreshItems;
end;

procedure TFormMacNotarizeResults.lvResultsCreateItemClass(Sender: TCustomListView; var ItemClass: TListItemClass);
begin
  ItemClass := TRequestListItem;
end;

procedure TFormMacNotarizeResults.lvResultsDblClick(Sender: TObject);
var
  li: TRequestListItem;
begin
  if not Assigned(lvResults.Selected) then begin
    Exit;
  end;
  li := TRequestListItem(lvResults.Selected);
  // TODO : show details
  // ShowMessage(li.RequestUUID);
  ShowNotarizeInfo(FAppleID, FKeyChainEntryName, li.RequestUUID);
end;

procedure TFormMacNotarizeResults.RefreshItems;
(*
Date                      RequestUUID                          Status  Status Code Status Message
------------------------- ------------------------------------ ------- ----------- ----------------
2018-11-23 10:14:26 +0000 CAFFEE00-1234-1234-1234-1234567890AB success 0           Package Approved
2018-11-22 08:16:56 +0000 CAFFEE00-1234-1234-1234-123456789001 invalid 2           Package Invalid
*)
const
  SDateRegExpr = '([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2} \+[0-9]{4})';
  SUUIDRegExpr = '([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})';
  SStatusRegExpr = '([a-z ]*)';
  SStatusCodeRegExpr = '([0-9]*|)';
  SStatusMessageRegExpr = '(\S[^\$]*|)';
var
  sl: TStringList;
  i: integer;
  r: TRegExpr;
  li: TRequestListItem;
begin
  sl := TStringList.Create;
  try
    MAWExecuteProcess('xcrun', Format('altool --notarization-history 0 -u "%s" -p "@keychain:%s"', [FAppleID, FKeyChainEntryName]), sl, [0]);
    lvResults.Items.BeginUpdate;
    try
      lvResults.Items.Clear;
      r := TRegExpr.Create(SDateRegExpr + '\s+' + SUUIDRegExpr + '\s+' + SStatusRegExpr + '\s*' + SStatusCodeRegExpr + '\s*' + SStatusMessageRegExpr);
      try
        for i := 0 to Pred(sl.Count) do begin
          if r.Exec(sl[i]) then begin
            li := TRequestListItem(lvResults.Items.Add);
            li.Date := r.Match[1];
            li.StatusMessage := r.Match[5];
            li.RequestUUID := r.Match[2];
            li.StatusCode := r.Match[4];
            li.StatusShort := r.Match[3];
          end;
        end;
      finally
        r.Free;
      end;
    finally
      lvResults.Items.EndUpdate;
    end;
  finally
    sl.Free;
  end;
end;

function TFormMacNotarizeResults.Execute(AAppleID, AKeyChainEntryName: string): boolean;
begin
  FAppleID := AAppleID;
  FKeyChainEntryName := AKeyChainEntryName;
  Result := ShowModal = mrOk;
end;

end.
