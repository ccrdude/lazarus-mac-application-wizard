unit MAWInfoPList;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  laz2_DOM,
  laz2_xmlread,
  laz2_xmlwrite;

type

  { TApplePList }

  TApplePList = class
  private
    FFilename: string;
    FDict: TDOMNode;
    FDocument: TXMLDocument;
    function GetKeyNode(AKey: string; out ANode: TDOMNode): boolean;
    function IsValid: boolean;
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadFromFile(AFilename: string);
    procedure SaveToFile;
    function GetStringArray(AKey: string; AList: TStrings): boolean;
    function GetStringData(AKey: string; out AText: string): boolean;
    function SetStringData(AKey: string; AText: string): boolean;
    function GetBooleanData(AKey: string; out AFlag: boolean): boolean;
    function SetBooleanData(AKey: string; AFlag: boolean): boolean;
    function EraseKey(AKey: string): boolean;
  end;

  { TAppleInfoPList }

  TAppleInfoPList = class(TApplePList)
  private
    FCFBundleSupportedPlatforms: TStringList;
    function GetCFBundleDisplayName: string;
    function GetCFBundleExecutable: string;
    function GetCFBundleIconFile: string;
    function GetCFBundleIdentifier: string;
    function GetCFBundleName: string;
    function GetCFBundlePackageType: string;
    function GetCFBundleShortVersionString: string;
    function GetCFBundleSupportedPlatforms: TStringList;
    function GetLSApplicationCategoryType: string;
    function GetLSMinimumSystemVersion: string;
    procedure SetCFBundleDisplayName(AValue: string);
    procedure SetCFBundleExecutable(AValue: string);
    procedure SetCFBundleIconFile(AValue: string);
    procedure SetCFBundleIdentifier(AValue: string);
    procedure SetCFBundleName(AValue: string);
    procedure SetCFBundlePackageType(AValue: string);
    procedure SetCFBundleShortVersionString(AValue: string);
    procedure SetLSApplicationCategoryType(AValue: string);
    procedure SetLSMinimumSystemVersion(AValue: string);
  public
    constructor Create;
    destructor Destroy; override;
    {
      CFBundleExecutable (String - iOS, macOS) identifies the name of the
      bundle’s main executable file. For an app, this is the app executable.
      For a loadable bundle, it is the binary that will be loaded dynamically
      by the bundle. For a framework, it is the shared library for the
      framework.

      For frameworks, the value of this key is required to be the same as the
      framework name, minus the .framework extension. If the keys are not
      the same, the target system may incur some launch-performance penalties.
      The value should not include any extension on the name.
    }
    property CFBundleExecutable: string read GetCFBundleExecutable write SetCFBundleExecutable;
    {
      CFBundleName (String - iOS, macOS) specifies the short name of the bundle,
      which may be displayed to users in situations such as the absence of a
      value for CFBundleDisplayName. This name should be less than 16
      characters long.
    }
    property CFBundleName: string read GetCFBundleName write SetCFBundleName;
    {
      CFBundleDisplayName (String - iOS, macOS) specifies the display name of
      the bundle, visible to users and used by Siri. If you support localized
      names for your bundle, include this key in your app’s Info.plist file
      and in the InfoPlist.strings files of your app’s language subdirectories.
      If you localize this key, include a localized version of the
      CFBundleName key as well.

      Because Siri uses the value of this key, always provide a value,
      whether or not you localize your app.

      In macOS, before displaying a localized name for your bundle, the
      Finder compares the value of this key against the actual name of your
      bundle in the file system. If the two names match, the Finder proceeds
      to display the localized name from the appropriate InfoPlist.strings
      file of your bundle. If the names do not match, the Finder displays
      the file-system name.
    }
    property CFBundleDisplayName: string read GetCFBundleDisplayName write SetCFBundleDisplayName;
    {
      CFBundleShortVersionString (String - iOS, macOS) specifies the release
      version number of the bundle, which identifies a released iteration of
      the app.

      The release version number is a string composed of three period-separated
      integers. The first integer represents major revision to the app, such as
      a revision that implements new features or major changes. The second
      integer denotes a revision that implements less prominent features. The
      third integer represents a maintenance release revision.

      @see(https://developer.apple.com/library/archive/documentation/General/Reference/InfoPlistKeyReference/Articles/CoreFoundationKeys.html#//apple_ref/doc/uid/20001431-111349)
    }
    property CFBundleShortVersionString: string read GetCFBundleShortVersionString write SetCFBundleShortVersionString;
    {
      CFBundlePackageType (String - iOS, macOS) identifies the type of the
      bundle and is analogous to the Mac OS 9 file type code. The value for
      this key consists of a four-letter code. The type code for apps is APPL;
      for frameworks, it is FMWK; for loadable bundles, it is BNDL. For
      loadable bundles, you can also choose a type code that is more specific
      than BNDL if you want.

      All bundles should provide this key. However, if this key is not
      specified, the bundle routines use the bundle extension to determine
      the type, falling back to the BNDL type if the bundle extension is
      not recognized.
    }
    property CFBundlePackageType: string read GetCFBundlePackageType write SetCFBundlePackageType;
    {
      CFBundleIdentifier (String - iOS, macOS) uniquely identifies the bundle.
      Each distinct app or bundle on the system must have a unique bundle ID.
      The system uses this string to identify your app in many ways. For
      example, the preferences system uses this string to identify the app
      for which a given preference applies; Launch Services uses the bundle
      identifier to locate an app capable of opening a particular file, using
      the first app it finds with the given identifier; in iOS, the bundle
      identifier is used in validating the app’s signature.

      The bundle ID string must be a uniform type identifier (UTI) that
      contains only alphanumeric (A-Z,a-z,0-9), hyphen (-), and period (.)
      characters. The string should also be in reverse-DNS format. For example,
      if your company’s domain is Ajax.com and you create an app named
      Hello, you could assign the string com.Ajax.Hello as your app’s bundle
      identifier.
    }
    property CFBundleIdentifier: string read GetCFBundleIdentifier write SetCFBundleIdentifier;
    property CFBundleIconFile: string read GetCFBundleIconFile write SetCFBundleIconFile;
    property CFBundleSupportedPlatforms: TStringList read GetCFBundleSupportedPlatforms;
    {
      Contains a string with the UTI that categorizes the app for the App Store.
      See LSApplicationCategoryType for details.

      @see(https://developer.apple.com/library/archive/documentation/General/Reference/InfoPlistKeyReference/Articles/LaunchServicesKeys.html#//apple_ref/doc/uid/TP40009250-SW8)
    }
    property LSApplicationCategoryType: string read GetLSApplicationCategoryType write SetLSApplicationCategoryType;
    {
      LSMinimumSystemVersion (String - macOS) indicates the minimum version of
      macOS required for this app to run. This string must be of the form n.n.n
      where n is a number. The first number is the major version number of the
      system. The second and third numbers are minor revision numbers. For
      example, to support macOS v10.4 and later, you would set the value of
      this key to “10.4.0”.

      @see(https://developer.apple.com/library/archive/documentation/General/Reference/InfoPlistKeyReference/Articles/LaunchServicesKeys.html#//apple_ref/doc/uid/20001431-113253)
    }
    property LSMinimumSystemVersion: string read GetLSMinimumSystemVersion write SetLSMinimumSystemVersion;
  end;

implementation

{ TAppleInfoPList }

function TAppleInfoPList.GetCFBundleDisplayName: string;
begin
  GetStringData('CFBundleDisplayName', Result);
end;

function TAppleInfoPList.GetCFBundleExecutable: string;
begin
  GetStringData('CFBundleExecutable', Result);
end;

function TAppleInfoPList.GetCFBundleIconFile: string;
begin
  GetStringData('CFBundleIconFile', Result);
end;

function TAppleInfoPList.GetCFBundleIdentifier: string;
begin
  GetStringData('CFBundleIdentifier', Result);
end;

function TAppleInfoPList.GetCFBundleName: string;
begin
  GetStringData('CFBundleName', Result);
end;

function TAppleInfoPList.GetCFBundlePackageType: string;
begin
  GetStringData('CFBundlePackageType', Result);
end;

function TAppleInfoPList.GetCFBundleShortVersionString: string;
begin
  GetStringData('CFBundleShortVersionString', Result);
end;

function TAppleInfoPList.GetCFBundleSupportedPlatforms: TStringList;
begin
  FCFBundleSupportedPlatforms.Clear;
  GetStringArray('CFBundleSupportedPlatforms', FCFBundleSupportedPlatforms);
  Result := FCFBundleSupportedPlatforms;
end;

function TAppleInfoPList.GetLSApplicationCategoryType: string;
begin
  GetStringData('LSApplicationCategoryType', Result);
end;

function TAppleInfoPList.GetLSMinimumSystemVersion: string;
begin
  GetStringData('LSMinimumSystemVersion', Result);
end;

procedure TAppleInfoPList.SetCFBundleDisplayName(AValue: string);
begin
  SetStringData('CFBundleDisplayName', AValue);
end;

procedure TAppleInfoPList.SetCFBundleExecutable(AValue: string);
begin
  SetStringData('CFBundleExecutable', AValue);
end;

procedure TAppleInfoPList.SetCFBundleIconFile(AValue: string);
begin
  SetStringData('CFBundleIconFile', AValue);
end;

procedure TAppleInfoPList.SetCFBundleIdentifier(AValue: string);
begin
  SetStringData('CFBundleIdentifier', AValue);
end;

procedure TAppleInfoPList.SetCFBundleName(AValue: string);
begin
  SetStringData('CFBundleName', AValue);
end;

procedure TAppleInfoPList.SetCFBundlePackageType(AValue: string);
begin
  SetStringData('CFBundlePackageType', AValue);
end;

procedure TAppleInfoPList.SetCFBundleShortVersionString(AValue: string);
begin
  SetStringData('CFBundleShortVersionString', AValue);
end;

procedure TAppleInfoPList.SetLSApplicationCategoryType(AValue: string);
begin
  SetStringData('LSApplicationCategoryType', AValue);
end;

procedure TAppleInfoPList.SetLSMinimumSystemVersion(AValue: string);
begin
  SetStringData('LSMinimumSystemVersion', AValue);
end;

constructor TAppleInfoPList.Create;
begin
  FCFBundleSupportedPlatforms := TStringList.Create;
end;

destructor TAppleInfoPList.Destroy;
begin
  FCFBundleSupportedPlatforms.Free;
  inherited Destroy;
end;

{ TApplePList }

function TApplePList.GetKeyNode(AKey: string; out ANode: TDOMNode): boolean;
var
  i: integer;
begin
  Result := False;
  ANode := nil;
  if not IsValid then begin
    Exit;
  end;
  for i := Pred(FDict.GetChildCount - 1) downto 0 do begin
    if FDict.ChildNodes[i].NodeName = 'key' then begin
      if FDict.ChildNodes[i].GetChildCount > 0 then begin
        if FDict.ChildNodes[i].ChildNodes[0] is TDOMText then begin
          if (TDOMText(FDict.ChildNodes[i].ChildNodes[0]).Data = AKey) then begin
            Result := True;
            ANode := FDict.ChildNodes[i];
          end;
        end;
      end;
    end;
  end;
end;

function TApplePList.IsValid: boolean;
begin
  FDict := nil;
  Result := False;
  if not Assigned(FDocument.DocumentElement) then begin
    Exit;
  end;
  if FDocument.DocumentElement.TagName <> 'plist' then begin
    Exit;
  end;
  if FDocument.DocumentElement.GetChildCount < 1 then begin
    Exit;
  end;
  FDict := FDocument.DocumentElement.ChildNodes[0];
  if FDict.NodeName <> 'dict' then begin
    FDict := nil;
    Exit;
  end;
  Result := True;

end;

constructor TApplePList.Create;
begin
  FDocument := nil;
end;

destructor TApplePList.Destroy;
begin
  FDocument.Free;
  inherited Destroy;
end;

procedure TApplePList.LoadFromFile(AFilename: string);
begin
  FDocument.Free;
  FFilename := AFilename;
  ReadXMLFile(FDocument, AFilename);
end;

procedure TApplePList.SaveToFile;
begin
  WriteXMLFile(FDocument, FFilename);
end;

function TApplePList.GetStringArray(AKey: string; AList: TStrings): boolean;
var
  nKey: TDOMNode;
  nArray: TDOMNode;
  nData: TDOMNode;
  nText: TDOMText;
  i: integer;
begin
  Result := False;
  AList.Clear;
  if not IsValid then begin
    Exit;
  end;
  if GetKeyNode(AKey, nKey) then begin
    nArray := nKey.NextSibling;
    if Assigned(nArray) then begin
      if nArray.NodeName = 'array' then begin
        for i := 0 to Pred(nArray.GetChildCount) do begin
          nData := nArray.ChildNodes[0];
          if (nData.NodeName = 'string') and (nData.GetChildCount = 1) then begin
            if nData.ChildNodes[0] is TDOMText then begin
              nText := TDOMText(nData.ChildNodes[0]);
              Alist.Add(nText.Data);
              Result := True;
            end;
          end;
        end;
      end;
    end;
  end;
end;

function TApplePList.GetStringData(AKey: string; out AText: string): boolean;
var
  nKey: TDOMNode;
  nData: TDOMNode;
  nText: TDOMText;
begin
  Result := False;
  AText := '';
  if not IsValid then begin
    Exit;
  end;
  if GetKeyNode(AKey, nKey) then begin
    nData := nKey.NextSibling;
    if Assigned(nData) then begin
      if nData.NodeName = 'string' then begin
        if nData.GetChildCount = 1 then begin
          if nData.ChildNodes[0] is TDOMText then begin
            nText := TDOMText(nData.ChildNodes[0]);
            AText := nText.Data;
            Result := True;
          end;
        end;
      end;
    end;
  end;
end;

function TApplePList.SetStringData(AKey: string; AText: string): boolean;
var
  nKey: TDOMNode;
  nData: TDOMNode;
  nTextKey: TDOMText;
  nTextData: TDOMText;
begin
  Result := False;
  if not IsValid then begin
    Exit;
  end;
  if GetKeyNode(AKey, nKey) then begin
    nData := nKey.NextSibling;
    if Assigned(nData) then begin
      if nData.NodeName = 'string' then begin
        if nData.GetChildCount = 1 then begin
          if nData.ChildNodes[0] is TDOMText then begin
            nTextData := TDOMText(nData.ChildNodes[0]);
            nTextData.Data := AText;
            Result := True;
          end;
        end;
      end;
    end;
  end;
  if not Result then begin
    nTextKey := FDocument.CreateTextNode(AKey);
    nTextData := FDocument.CreateTextNode(AText);
    nKey := FDocument.CreateElement('key');
    nData := FDocument.CreateElement('string');
    FDict.AppendChild(nKey);
    FDict.AppendChild(nData);
    nKey.AppendChild(nTextKey);
    nData.AppendChild(nTextData);
  end;
end;

function TApplePList.GetBooleanData(AKey: string; out AFlag: boolean): boolean;
var
  nKey: TDOMNode;
  nData: TDOMNode;
begin
  Result := False;
  AFlag := False;
  if not IsValid then begin
    Exit;
  end;
  if GetKeyNode(AKey, nKey) then begin
    nData := nKey.NextSibling;
    if Assigned(nData) then begin
      if nData.NodeName = 'true' then begin
        Result := True;
        AFlag := True;
      end else if nData.NodeName = 'false' then begin
        Result := True;
        AFlag := False;
      end;
    end;
  end;
end;

function TApplePList.SetBooleanData(AKey: string; AFlag: boolean): boolean;
var
  nKey: TDOMNode;
  nData: TDOMNode;
  nNew: TDOMNode;
  nTextKey: TDOMText;
  nTextData: TDOMText;
begin
  Result := False;
  if not IsValid then begin
    Exit;
  end;
  if GetKeyNode(AKey, nKey) then begin
    nData := nKey.NextSibling;
    if Assigned(nData) then begin
      if (nData.NodeName <> 'false') and (not AFlag) then begin
        nNew := FDocument.CreateElement('false');
        FDict.ReplaceChild(nNew, nData);
      end else if (nData.NodeName <> 'true') and (AFlag) then begin
        nNew := FDocument.CreateElement('true');
        FDict.ReplaceChild(nNew, nData);
      end;
      Result := True;
    end;
  end;
  if not Result then begin
    nTextKey := FDocument.CreateTextNode(AKey);
    nKey := FDocument.CreateElement('key');
    nData := FDocument.CreateElement(BoolToStr(AFlag, 'true', 'false'));
    FDict.AppendChild(nKey);
    FDict.AppendChild(nData);
    nKey.AppendChild(nTextKey);
  end;
end;

function TApplePList.EraseKey(AKey: string): boolean;
var
  nKey: TDOMNode;
  nData: TDOMNode;
begin
  Result := False;
  if not IsValid then begin
    Exit;
  end;
  if GetKeyNode(AKey, nKey) then begin
    nData := nKey.NextSibling;
    if Assigned(nData) then begin
      FDict.RemoveChild(nKey);
      FDict.RemoveChild(nData);
      Result := True;
    end;
  end;
end;

end.
