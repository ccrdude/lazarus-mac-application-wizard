unit MAWFormEntitlements;

// https://developer.apple.com/library/archive/documentation/Miscellaneous/Reference/EntitlementKeyReference/Chapters/AboutEntitlements.html

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ButtonPanel,
  StdCtrls,
  MAWInfoPList;

type

  { TFormMacEntitlementsEditor }

  TFormMacEntitlementsEditor = class(TForm)
    buttonsDialog: TButtonPanel;
    cb_com_apple_developer_payment_pass_provisioning: TCheckBox;
    cb_com_apple_developer_passkit_pass_presentation_suppression: TCheckBox;
    cb_com_apple_security_assets_movies_read_only: TCheckBox;
    cb_com_apple_security_print: TCheckBox;
    cb_com_apple_security_device_audio_video_bridging: TCheckBox;
    cb_com_apple_security_device_bluetooth: TCheckBox;
    cb_com_apple_security_device_firewire: TCheckBox;
    cb_com_apple_security_device_serial: TCheckBox;
    cb_com_apple_security_device_usb: TCheckBox;
    cb_com_apple_security_files_user_selected_read_only: TCheckBox;
    cb_com_apple_security_device_camera: TCheckBox;
    cb_com_apple_security_files_user_selected_read_write: TCheckBox;
    cb_com_apple_security_files_downloads_read_write: TCheckBox;
    cb_com_apple_security_assets_movies_read_write: TCheckBox;
    cb_com_apple_security_assets_music_read_only: TCheckBox;
    cb_com_apple_security_assets_music_read_write: TCheckBox;
    cb_com_apple_security_assets_pictures_read_only: TCheckBox;
    cb_com_apple_security_assets_pictures_read_write: TCheckBox;
    cb_com_apple_security_device_audio_input: TCheckBox;
    cb_com_apple_security_network_client: TCheckBox;
    cb_com_apple_security_app_sandbox: TCheckBox;
    cb_com_apple_security_application_groups: TCheckBox;
    cb_com_apple_security_files_bookmarks_document_scope: TCheckBox;
    cb_com_apple_security_personal_information_location: TCheckBox;
    cb_com_apple_security_network_server: TCheckBox;
    cb_com_apple_security_files_bookmarks_app_scope: TCheckBox;
    cb_com_apple_security_personal_information_addressbook: TCheckBox;
    cb_com_apple_security_personal_information_calendars: TCheckBox;
    gbSandbox1: TGroupBox;
    gbApplePay: TGroupBox;
    gbSandboxNetwork: TGroupBox;
    gbSandboxBookmarkURL: TGroupBox;
    gbSandboxPersonalInformationAccess: TGroupBox;
    gbSandboxStandardLocations: TGroupBox;
    gbSandboxHardwareAccess: TGroupBox;
    label_com_apple_developer_in_app_payments: TLabel;
    label_com_apple_developer_pass_type_identifiers: TLabel;
    memo_com_apple_developer_pass_type_identifiers: TMemo;
    memo_com_apple_developer_in_app_payments: TMemo;
    pcEntitlements: TPageControl;
    sbSandbox: TScrollBox;
    sbApplePay: TScrollBox;
    tabSandbox: TTabSheet;
    tabApplePay: TTabSheet;
    tabICloudStorage: TTabSheet;
    tabPushNotifications: TTabSheet;
    tabTemporaryExceptions: TTabSheet;
  private
    FFilename: string;
    FEntitlements: TApplePList;
    procedure LoadCheckbox(ACheckbox: TCheckbox);
    procedure SaveCheckbox(ACheckbox: TCheckbox);
    procedure LoadData;
    procedure SaveData;
  public
    function Execute(AFilename: string): boolean;
  end;


function EditEntitlements(AFilename: string): boolean;

implementation

function EditEntitlements(AFilename: string): boolean;
var
  form: TFormMacEntitlementsEditor;
begin
  form := TFormMacEntitlementsEditor.Create(Application.MainForm);
  try
    Result := form.Execute(AFilename);
  finally
    form.Free;
  end;
end;

{$R *.lfm}

{ TFormMacEntitlementsEditor }

procedure TFormMacEntitlementsEditor.LoadCheckbox(ACheckbox: TCheckbox);
var
  b: boolean;
begin
  ACheckbox.State := cbGrayed;
  if FEntitlements.GetBooleanData(ACheckbox.Hint, b) then
  begin
    if b then
    begin
      ACheckbox.State := cbChecked;
    end
    else
    begin
      ACheckbox.State := cbUnchecked;
    end;
  end;
end;

procedure TFormMacEntitlementsEditor.SaveCheckbox(ACheckbox: TCheckbox);
begin
  case ACheckbox.State of
    cbChecked:
    begin
      FEntitlements.SetBooleanData(ACheckbox.Hint, True);
    end;
    cbUnchecked:
    begin
      FEntitlements.SetBooleanData(ACheckbox.Hint, False);
    end;
    cbGrayed:
    begin
      FEntitlements.EraseKey(ACheckbox.Hint);
    end;
  end;
end;

procedure TFormMacEntitlementsEditor.LoadData;

  procedure LoadFromGroupBox(AGroupBox: TGroupBox);
  var
    i: integer;
  begin
    for i := 0 to Pred(AGroupBox.ControlCount) do
    begin
      if AGroupBox.Controls[i] is TCheckbox then
      begin
        LoadCheckbox(TCheckbox(AGroupBox.Controls[i]));
      end;
    end;
  end;

  procedure LoadFromScrollBox(AScrollBox: TScrollBox);
  var
    i: integer;
  begin
    for i := 0 to Pred(AScrollBox.ControlCount) do
    begin
      if AScrollBox.Controls[i] is TGroupBox then
      begin
        LoadFromGroupBox(TGroupBox(AScrollBox.Controls[i]));
      end;
    end;
  end;

  procedure LoadFromTab(ATab: TTabSheet);
  begin
    if ATab.ControlCount > 0 then
    begin
      if ATab.Controls[0] is TScrollBox then
      begin
        LoadFromScrollBox(TScrollBox(ATab.Controls[0]));
      end;
    end;
  end;

var
  i: integer;
begin
  for i := 0 to Pred(pcEntitlements.PageCount) do
  begin
    LoadFromTab(pcEntitlements.Pages[i]);
  end;
end;

procedure TFormMacEntitlementsEditor.SaveData;

  procedure SaveToGroupBox(AGroupBox: TGroupBox);
  var
    i: integer;
  begin
    for i := 0 to Pred(AGroupBox.ControlCount) do
    begin
      if AGroupBox.Controls[i] is TCheckbox then
      begin
        SaveCheckbox(TCheckbox(AGroupBox.Controls[i]));
      end;
    end;
  end;

  procedure SaveToScrollBox(AScrollBox: TScrollBox);
  var
    i: integer;
  begin
    for i := 0 to Pred(AScrollBox.ControlCount) do
    begin
      if AScrollBox.Controls[i] is TGroupBox then
      begin
        SaveToGroupBox(TGroupBox(AScrollBox.Controls[i]));
      end;
    end;
  end;

  procedure SaveToTab(ATab: TTabSheet);
  begin
    if ATab.ControlCount > 0 then
    begin
      if ATab.Controls[0] is TScrollBox then
      begin
        SaveToScrollBox(TScrollBox(ATab.Controls[0]));
      end;
    end;
  end;

var
  i: integer;
begin
  for i := 0 to Pred(pcEntitlements.PageCount) do
  begin
    SaveToTab(pcEntitlements.Pages[i]);
  end;
end;

function TFormMacEntitlementsEditor.Execute(AFilename: string): boolean;
begin
  FFilename := AFilename;
  FEntitlements := TApplePList.Create;
  try
    FEntitlements.LoadFromFile(AFilename);
    LoadData;
    Result := (ShowModal = mrOk);
    if Result then
    begin
      SaveData;
      FEntitlements.SaveToFile;
    end;
  finally
    FEntitlements.Free;
  end;
end;

end.
