unit MAWHandling;

{$mode objfpc}{$H+}
{$modeswitch objectivec1}

interface

uses
  Classes,
  SysUtils,
  CocoaAll;

type
  TLazarusMacAppBundleWizardOption = (mabwoAppStore, mabwoNotarize, mabwoNoDeepSign);
  TLazarusMacAppBundleWizardOptions = set of TLazarusMacAppBundleWizardOption;

  TLazarusMapAppWizardLogLineType = (lltHeader, lltInfo, lltSuccess, lltError);

  { TLazarusMacAppWizard }

  TLazarusMacAppWizard = class
  private
    FAppBundlePath: string;
    FAppleID: string;
    FBinaryPath: string;
    FSigningIdentityDeveloperIDAppBundle: string;
    FSigningIdentityThirdPartyMacDevAppBundle: string;
    FEntitlementsPath: string;
    FInstallerPath: string;
    FKeychainEntryName: string;
    FLastActionLog: TStringList;
    FLazarusProjectFile: string;
    FOptions: TLazarusMacAppBundleWizardOptions;
    FPackageID: string;
    FSigningIdentityThirdPartyMacDevInstaller: string;
    FVerbosityLevel: integer;
    function GetAppBundleCodeSigningParameters(ATarget: string; AUseEntitlements: boolean = True): string;
    function GetInstallerCodeSigningParameters(ARenamedInstaller: string): string;
    function GetCreateInstallerParameters: string;
    function GetInstallerNotarizingParameters: string;
    function ExecuteProcess(AProcess, AParameters: string; AGoodExitCodes: array of integer): boolean;
    procedure SetAppBundlePath(AValue: string);
    procedure LogLine(AType: TLazarusMapAppWizardLogLineType; AText: string; ALogLevel: integer = 1);
  public
    constructor Create;
    destructor Destroy; override;
    property BinaryPath: string read FBinaryPath write FBinaryPath;
    property EntitlementsPath: string read FEntitlementsPath write FEntitlementsPath;
    property AppBundlePath: string read FAppBundlePath write SetAppBundlePath;
    property InstallerPath: string read FInstallerPath write FInstallerPath;
    property SigningIdentityDeveloperIDAppBundle: string read FSigningIdentityDeveloperIDAppBundle write FSigningIdentityDeveloperIDAppBundle;
    property SigningIdentityThirdPartyMacDevAppBundle: string read FSigningIdentityThirdPartyMacDevAppBundle write FSigningIdentityThirdPartyMacDevAppBundle;
    property SigningIdentityThirdPartyMacDevInstaller: string read FSigningIdentityThirdPartyMacDevInstaller write FSigningIdentityThirdPartyMacDevInstaller;
    property PackageID: string read FPackageID write FPackageID;
    property AppleID: string read FAppleID write FAppleID;
    property LazarusProjectFile: string read FLazarusProjectFile write FLazarusProjectFile;
    property KeychainEntryName: string read FKeychainEntryName write FKeychainEntryName;

    {
      ForgetPackageId tells the system to forget the package, to avoid confusion
      with copies existing in other paths.

      It executes and evaluates the output of:
      pkgutil --forget <package id>
    }
    function ForgetPackageId: boolean;
    {
      CopyExecutableIntoBundle copies the latest application binary into the
      application bundle folder.
    }
    function CopyExecutableIntoBundle: boolean;
    {
      RemoveExtendedAttributes removes extended Finder attributes from the
      application bundle. This can be useful if for example you've copied
      another icon for test purposes.

      It executes and evaluates the output of:
      xattr -cr "<application bundle>"

      @see(http://forum.lazarus.freepascal.org/index.php/topic,43280.msg302652.html#msg302652)
    }
    function RemoveExtendedAttributes: boolean;
    {
      RemoveMetaFiles removes Icon and .DS_Store files from the application
      bundle.
    }
    function RemoveMetaFiles: boolean;
    {
      ChangePermissions sets the file of the application bundle to 755.

      If you have less permissions, it could happen that the installer creation
      would result in an inaccessible application bundle, because that changes
      the owner, but not the permissions.
    }
    function ChangePermissions: boolean;
    {
      Info.plist can exist in binary or text (XML) format.
      For this tool to process it, it needs to be in XML format.

      It executes and evaluates the output of:
      plutil -convert xml1 Info.plist
    }
    function InfoPListBinaryToXML: boolean;
    {
      TestInfoPList checks if a Info.plist exists and has some required fields.

      It will complain if CFBundleSupportedPlatforms is not set, since this
      is required if you want to have the application bundle notarized.

      It will also copy over changed settings from a Lazarus Project File (.lpi).
    }
    function TestInfoPList: boolean;
    {
      TestIcon checks if an icon is set and it includes the required icon
      sizes.
    }
    function TestIcon: boolean;
    {
      TestEntitlements tests if the provided entitlements file has a valid
      format.

      It executes and evaluates the output of:
      plutil "<entitlements file>"
    }
    function TestEntitlements: boolean;
    {
      SignApplicationBundle will sign the application bundle.
      For the store, it needs a "Developer ID Application" identity.

      It executes and evaluates the output of:
      codesign --verbose --display --force --sign "<signing identity>" --options runtime "<application bundle>" --timestamp --deep --entitlements "<entitlements file>"
    }
    function SignApplicationBundle: boolean;
    {
      TestGateKeeperAppBundle runs a check to see if the app bundle would pass
      GateKeeper when installing on a customers machine.

      It executes and evaluates the output of:
      spctl --assess --type execute -vv "<application bundle>"
    }
    function TestGateKeeperAppBundle: boolean;
    function TestGateKeeperInstaller: boolean;
    {
      CreateInstaller packages the app bundle into a .pkg installer,
      as required by the store.

      It executes and evaluates the output of:
      productbuild --component "<application bundle>" /Applications "<installer path>"
    }
    function CreateInstaller: boolean;
    {
      SignInstaller codesignes the installer so that it'll show as signed
      on the customers machine.
      For the store, it needs a "3rd Party Mac Developer Installer" identity.

      It executes and evaluates the output of:
      productsign --timestamp --sign "<signing identity>" "<unsigned installer path>" "<signed installer path>"
    }
    function SignInstaller: boolean;
    {
      NotarizeInstaller starts the notarize process in which Apple tests
      the package for malware and other issues that could prevent it from
      being accepted into the store.

      It executes and evaluates the output of:
      xcrun altool --notarize-app --primary-bundle-id "<package id>" --username "<apple id>" --password "@keychain:%s" --file "<installer path>"
    }
    function NotarizeInstaller: boolean;

    property LastActionLog: TStringList read FLastActionLog;
    property VerbosityLevel: integer read FVerbosityLevel write FVerbosityLevel;
    property Options: TLazarusMacAppBundleWizardOptions read FOptions write FOptions;
  end;

function MAWExecuteProcess(AProcess, AParameters: string; AOutput: TStrings; AGoodExitCodes: array of integer; ADelimiter: char = ' '): boolean;

implementation

uses
  {$IFDEF Darwin}
  BaseUnix,
  Unix,
  {$ENDIF Darwin}
  FileUtil,
  Process,
  MAWInfoPList,
  MAWLazarusProjectFile;

resourcestring
  rsBrowseAddingToStack = 'Adding to stack: %s';
  rsBrowsePathLevel = 'Looking inside: %s';
  rsBrowseTesting = 'Testing: "%s"';
  rsChangePermissionsFailure = 'Permissions not all changed!';
  rsChangePermissionsFileFailure = 'fpChmod failed for file %s';
  rsChangePermissionsHeader = 'Change Permissions';
  rsChangePermissionsSuccess = 'Permissions set.';
  rsCopyExecutableIntoBundleFailure = 'Unable to copy binary';
  rsCopyExecutableIntoBundleHeader = 'Copy Executable Into App Bundle';
  rsCopyExecutableIntoBundleNewPath = 'New Binary Path: %s';
  rsCopyExecutableIntoBundleRemoveOldFailure = 'Unable to remove old binary';
  rsCopyExecutableIntoBundleRemoveOldSuccess = 'Removed old binary';
  rsCopyExecutableIntoBundleSuccess = 'Copied binary.';
  rsCreateInstallerFailure = 'Failed to create Installer Package.';
  rsCreateInstallerHeader = 'Create Installer';
  rsCreateInstallerRemoveFailure = 'Failed to remove: %s';
  rsCreateInstallerRemoveSuccess = 'Removed: %s';
  rsCreateInstallerSuccess = 'Installer Package created.';
  rsExternalProcessExitCode = 'Exit Code: %d';
  rsForgetPackageIdFailure = 'Package ID not forgotten!';
  rsForgetPackageIdHeader = 'Forget Package';
  rsForgetPackageIdSuccess = 'Package ID forgotten.';
  rsInfoAppBundle = 'App Bundle: %s';
  rsInfoAppleID = 'Apple ID: %s';
  rsInfoEntitlements = 'Entitlements: %s';
  rsInfoExecutable = 'Executable: %s';
  rsInfoInstaller = 'Installer: %s';
  rsInfoKeyChainEntryName = 'Key Chain Entry Name: %s';
  rsInfoPackageId = 'Package Id: %s';
  rsInfoPListBinaryToXMLFailure = 'Unable to convert Info.plist to XML format!';
  rsInfoPListBinaryToXMLHeader = 'Converting Info.plist to XML format';
  rsInfoPListBinaryToXMLSuccess = 'Info.plist now is in XML format.';
  rsInfoSigningIdentityAppBundle = 'Signing Identity (App Bundles): %s';
  rsNotarizeInstallerErrorNoAppleID = 'Unable to notarize; please specify Apple ID!';
  rsNotarizeInstallerErrorNoInstallerPath = 'Unable to notarize; you need to specify the installer output path!';
  rsNotarizeInstallerErrorNoKeyChainEntryName = 'Unable to notarize; please specify Keychain Entry Name!';
  rsNotarizeInstallerFailure = 'Could not start installer notarization.';
  rsNotarizeInstallerHeader = 'Notarize Installer';
  rsNotarizeInstallerSuccess = 'Installer notarization started.';
  rsRemoveExtendedAttributesFailure = 'Unable to remove Extended Attributes!';
  rsRemoveExtendedAttributesHeader = 'Removing Extended Attributes';
  rsRemoveExtendedAttributesSuccess = 'Extended Attributes removed.';
  rsRemoveMetaFilesFailure = 'Unable to remove meta files!';
  rsRemoveMetaFilesHeader = 'Remove Meta Files';
  rsRemoveMetaFilesSuccess = 'Meta files removed.';
  rsSignApplicationBundleFailure = 'Failed to sign App Bundle.';
  rsSignApplicationBundleHeader = 'Sign App Bundle';
  rsSignApplicationBundleSuccess = 'App Bundle signed.';
  rsSignInstallerFailure = 'Failed to sign Installer.';
  rsSignInstallerHeader = 'Sign Installer';
  rsSignInstallerRenameFailure = 'Failed to rename installer.';
  rsSignInstallerSuccess = 'Installer signed.';
  rsTestEntitlementsFailure = 'Entitlements failed!';
  rsTestEntitlementsHeader = 'Test Entitlements';
  rsTestEntitlementsSuccess = 'Entitlements are okay.';
  rsTestGateKeeperFailure = 'GateKeeper incompatible!';
  rsTestGateKeeperHeader = 'GateKeeper test';
  rsTestGateKeeperSuccess = 'GateKeeper compatible.';
  rsTestIconErrorMissing1024 = 'Icon in 512x512@2x missing!';
  rsTestIconErrorMissing512 = 'Icon in 512x512 missing!';
  rsTestIconException = 'Icon exception: %s';
  rsTestIconFileMissing = 'Icon missing: %s';
  rsTestIconHeader = 'Testing Icon';
  rsTestIconIconsMissing = 'Icons in 512x512 and 512x512@2x found.';
  rsTestIconInfoIconFilename = 'Icon: %s';
  rsTestIconInfoImageRepresentation = 'NSImage representations[%d]: %d x %d (%' + 'd bits)';
  rsTestIconInfoImageRepresentationCount = 'NSImage representations.Count: %d';
  rsTestInfoPListCFBundleDisplayNameUpdatedFromLpi = 'Updated Info.plist''s CFBundleDisplayName from .lpi file';
  rsTestInfoPlistCFBundleExecutableUpdated = 'Info.plist CFBundleExecutable changed from "%s" to "%s".';
  rsTestInfoPListCFBundleIdentifierUpdatedFromLpi = 'Updated Info.plist''s CFBundleIdentifier from .lpi file';
  rsTestInfoPlistCFBundlePackageTypeUpdated = 'Info.plist CFBundlePackageType changed from "%s" to "APPL".';
  rsTestInfoPlistCFBundleShortVersionStringUpdatedFromLpi = 'Updated Info.plist''s CFBundleShortVersionString from .lpi file';
  rsTestInfoPListFieldMissing = 'Info.plist %s missing!';
  rsTestInfoPListFileExists = 'Info.plist exists.';
  rsTestInfoPListFileMissing = 'Info.plist missing!';
  rsTestInfoPListHeader = 'Testing Info.plist';
  rsTestInfoPListInfoField = 'Info.plist %s: %s';
  rsTestInfoPListLpiUniformTypeIdentifier = '.lpi: General/XPManifest/TextName: %s';
  rsTestInfoPListLpiVersionProductName = '.lpi: VersionInfo/StringTable/ProductName: %s';
  rsTestInfoPListLpiVersionProductVersion = '.lpi: VersionInfo/StringTable/ProductVersion: %s';

function MAWExecuteProcess(AProcess, AParameters: string; AOutput: TStrings; AGoodExitCodes: array of integer; ADelimiter: char): boolean;

const
  BUF_SIZE = 2048;

var
  p: TProcess;
  ms: TMemoryStream;
  iBytesRead: longint;
  sl: TStringList;
  Buffer: array[1..BUF_SIZE] of byte;
  i: integer;
begin
  Result := False;
  p := TProcess.Create(nil);
  try
    p.Executable := AProcess;
    p.Parameters.Delimiter := ADelimiter;
    p.Parameters.StrictDelimiter := True;
    p.Parameters.DelimitedText := AParameters;
    p.Options := [poUsePipes];
    p.Execute;

    ms := TMemoryStream.Create;
    try
      repeat
        iBytesRead := p.Output.Read(Buffer{%H-}, BUF_SIZE);
        ms.Write(Buffer, iBytesRead);
      until iBytesRead = 0;
      sl := TStringList.Create;
      try
        ms.Seek(0, soFromBeginning);
        sl.LoadFromStream(ms);
        for i := 0 to Pred(sl.Count) do begin
          AOutput.Add(sl[i]);
        end;
      finally
        sl.Free;
      end;

      ms.SetSize(0);
      repeat
        iBytesRead := p.Stderr.Read(Buffer, BUF_SIZE);
        ms.Write(Buffer, iBytesRead);
      until iBytesRead = 0;
      sl := TStringList.Create;
      try
        ms.Seek(0, soFromBeginning);
        sl.LoadFromStream(ms);
        for i := 0 to Pred(sl.Count) do begin
          AOutput.Add(sl[i]);
        end;
      finally
        sl.Free;
      end;

    finally
      ms.Free;
    end;
    for i := Low(AGoodExitCodes) to High(AGoodExitCodes) do begin
      if AGoodExitCodes[i] = p.ExitCode then begin
        Result := True;
        break;
      end;
    end;
  finally
    p.Free;
  end;
end;

{ TLazarusMacAppWizard }

function TLazarusMacAppWizard.ExecuteProcess(AProcess, AParameters: string; AGoodExitCodes: array of integer): boolean;

const
  BUF_SIZE = 2048; // Buffer size for reading the output in chunks

var
  p: TProcess;
  ms: TMemoryStream;
  iBytesRead: longint;
  sl: TStringList;
  Buffer: array[1..BUF_SIZE] of byte;
  i: integer;
begin
  Result := False;
  p := TProcess.Create(nil);
  try
    LogLine(lltInfo, AProcess + ' ' + AParameters, 2);
    p.Executable := AProcess;
    p.Parameters.Delimiter := ' ';
    p.Parameters.StrictDelimiter := True;
    p.Parameters.DelimitedText := AParameters;
    p.Options := [poUsePipes];
    p.Execute;

    ms := TMemoryStream.Create;
    try
      repeat
        iBytesRead := p.Output.Read(Buffer{%H-}, BUF_SIZE);
        ms.Write(Buffer, iBytesRead);
      until iBytesRead = 0;
      sl := TStringList.Create;
      try
        ms.Seek(0, soFromBeginning);
        sl.LoadFromStream(ms);
        for i := 0 to Pred(sl.Count) do begin
          LogLine(lltInfo, sl[i]);
        end;
      finally
        sl.Free;
      end;

      ms.SetSize(0);
      repeat
        iBytesRead := p.Stderr.Read(Buffer, BUF_SIZE);
        ms.Write(Buffer, iBytesRead);
      until iBytesRead = 0;
      sl := TStringList.Create;
      try
        ms.Seek(0, soFromBeginning);
        sl.LoadFromStream(ms);
        for i := 0 to Pred(sl.Count) do begin
          LogLine(lltError, sl[i]);
        end;
      finally
        sl.Free;
      end;

    finally
      ms.Free;
    end;
    if p.ExitCode = 0 then begin
      LogLine(lltInfo, Format(rsExternalProcessExitCode, [p.ExitCode]), 2);
    end else begin
      LogLine(lltInfo, Format(rsExternalProcessExitCode, [p.ExitCode]), 1);
    end;
    for i := Low(AGoodExitCodes) to High(AGoodExitCodes) do begin
      if AGoodExitCodes[i] = p.ExitCode then begin
        Result := True;
        break;
      end;
    end;
  finally
    p.Free;
  end;
end;

procedure TLazarusMacAppWizard.SetAppBundlePath(AValue: string);
var
  info: TAppleInfoPList;
  sFilename: string;
begin
  FAppBundlePath := AValue;
  sFilename := IncludeTrailingPathDelimiter(FAppBundlePath) + 'Contents/Info.plist';
  if FileExists(sFilename) then begin
    info := TAppleInfoPList.Create;
    try
      info.LoadFromFile(sFilename);
      FPackageID := info.CFBundleIdentifier;
    finally
      info.Free;
    end;
  end;
end;

procedure TLazarusMacAppWizard.LogLine(AType: TLazarusMapAppWizardLogLineType; AText: string; ALogLevel: integer);
var
  s: string;
begin
  if (ALogLevel > VerbosityLevel) then begin
    Exit;
  end;
  case AType of
    lltHeader: s := '>>> ';
    lltInfo: s := '[i] ';
    lltError: s := '[-] ';
    lltSuccess: s := '[+] ';
    else
      s := '    ';
  end;
  FLastActionLog.Add(s + AText);
end;

constructor TLazarusMacAppWizard.Create;
begin
  FLastActionLog := TStringList.Create;
  FVerbosityLevel := 1;
  FOptions := [mabwoAppStore];
end;

destructor TLazarusMacAppWizard.Destroy;
begin
  FLastActionLog.Free;
  inherited Destroy;
end;

function TLazarusMacAppWizard.GetAppBundleCodeSigningParameters(ATarget: string; AUseEntitlements: boolean): string;
var s: string;
begin
  if mabwoAppStore in FOptions then begin
    s := FSigningIdentityThirdPartyMacDevAppBundle;
  end else begin
    s := FSigningIdentityDeveloperIDAppBundle;
  end;
  Result := Format('--verbose --display --force --sign "%s" --options runtime "%s" --timestamp', [s, ATarget]);
  if not (mabwoNoDeepSign in FOptions) then begin
    Result += ' --deep';
  end;
  if (Length(FEntitlementsPath) > 0) and (AUseEntitlements) then begin
    Result += Format(' --entitlements "%s"', [FEntitlementsPath]);
  end;
end;

function TLazarusMacAppWizard.GetInstallerCodeSigningParameters(ARenamedInstaller: string): string;
begin
  Result := Format('--timestamp --sign "%s" "%s" "%s"', [FSigningIdentityThirdPartyMacDevInstaller, ARenamedInstaller, FInstallerPath]);
end;

function TLazarusMacAppWizard.GetCreateInstallerParameters: string;
begin
  Result := Format('--component "%s" /Applications "%s" ', [FAppBundlePath, FInstallerPath]);
end;

function TLazarusMacAppWizard.GetInstallerNotarizingParameters: string;
begin
  Result := Format('altool --notarize-app --primary-bundle-id "%s" --username "%s" --password "@keychain:%s" --file "%s"',
    [FPackageID, FAppleID, FKeychainEntryName, FInstallerPath]);
end;

function TLazarusMacAppWizard.CopyExecutableIntoBundle: boolean;
var
  sTargetFilename: string;
begin
  Result := False;
  FLastActionLog.Clear;
  LogLine(lltHeader, rsCopyExecutableIntoBundleHeader);
  LogLine(lltInfo, Format(rsInfoExecutable, [FBinaryPath]), 2);
  LogLine(lltInfo, Format(rsInfoAppBundle, [FAppBundlePath]), 2);
  sTargetFilename := IncludeTrailingPathDelimiter(FAppBundlePath) + 'Contents/MacOS/' + ExtractFileName(FBinaryPath);
  LogLine(lltInfo, Format(rsCopyExecutableIntoBundleNewPath, [sTargetFilename]), 2);
  if FileExists(sTargetFilename) then begin
    if DeleteFile(sTargetFilename) then begin
      LogLine(lltSuccess, rsCopyExecutableIntoBundleRemoveOldSuccess, 2);
    end else begin
      LogLine(lltError, rsCopyExecutableIntoBundleRemoveOldFailure);
      Exit;
    end;
  end;
  if CopyFile(FBinaryPath, sTargetFilename) then begin
    LogLine(lltSuccess, rsCopyExecutableIntoBundleSuccess);
    Result := True;
  end else begin
    LogLine(lltError, rsCopyExecutableIntoBundleFailure);
    Exit;
  end;
end;

function TLazarusMacAppWizard.TestInfoPList: boolean;

  procedure TestFieldSet(AFieldName, AData: string);
  begin
    if Length(AData) > 0 then begin
      LogLine(lltSuccess, Format(rsTestInfoPListInfoField, [AFieldName, AData]), 2);
    end else begin
      LogLine(lltError, Format(rsTestInfoPListFieldMissing, [AFieldName]));
      Exit;
    end;
  end;

var
  sFilename: string;
  f: TAppleInfoPList;
  lpi: TLazarusProjectFile;
begin
  Result := False;
  FLastActionLog.Clear;
  LogLine(lltHeader, rsTestInfoPListHeader);
  sFilename := IncludeTrailingPathDelimiter(FAppBundlePath) + 'Contents/Info.plist';
  if FileExists(sFilename) then begin
    LogLine(lltSuccess, rsTestInfoPListFileExists);
  end else begin
    LogLine(lltError, rsTestInfoPListFileMissing);
    Exit;
  end;
  f := TAppleInfoPList.Create;
  try
    f.LoadFromFile(sFilename);
    if f.CFBundlePackageType <> 'APPL' then begin
      LogLine(lltInfo, Format(rsTestInfoPlistCFBundlePackageTypeUpdated, [f.CFBundlePackageType]));
      f.CFBundlePackageType := 'APPL';
    end;
    if f.CFBundleExecutable <> ExtractFileName(BinaryPath) then begin
      LogLine(lltInfo, Format(rsTestInfoPlistCFBundleExecutableUpdated, [f.CFBundleExecutable, ExtractFileName(BinaryPath)]));
      f.CFBundleExecutable := ExtractFileName(BinaryPath);
    end;
    if FileExists(FLazarusProjectFile) then begin
      lpi := TLazarusProjectFile.Create;
      try
        lpi.LoadFromFile(FLazarusProjectFile);
        if Length(lpi.VersionProductName) > 0 then begin
          LogLine(lltInfo, Format(rsTestInfoPListLpiVersionProductName, [lpi.VersionProductName]), 2);
          if lpi.VersionProductName <> f.CFBundleName then begin
            LogLine(lltInfo, 'Updated Info.plist''s CFBundleName from .lpi file');
            f.CFBundleName := lpi.VersionProductName;
            f.SaveToFile;
          end;
          if lpi.VersionProductName <> f.CFBundleDisplayName then begin
            LogLine(lltInfo, rsTestInfoPListCFBundleDisplayNameUpdatedFromLpi);
            f.CFBundleDisplayName := lpi.VersionProductName;
            f.SaveToFile;
          end;
        end;
        if Length(lpi.VersionProductVersion) > 0 then begin
          LogLine(lltInfo, Format(rsTestInfoPListLpiVersionProductVersion, [lpi.VersionProductVersion]), 2);
          if lpi.VersionProductVersion <> f.CFBundleShortVersionString then begin
            LogLine(lltInfo, rsTestInfoPlistCFBundleShortVersionStringUpdatedFromLpi);
            f.CFBundleShortVersionString := lpi.VersionProductVersion;
            f.SaveToFile;
          end;
        end;
        if Length(lpi.UniformTypeIdentifier) > 0 then begin
          LogLine(lltInfo, Format(rsTestInfoPListLpiUniformTypeIdentifier, [lpi.UniformTypeIdentifier]), 2);
          if lpi.UniformTypeIdentifier <> f.CFBundleIdentifier then begin
            LogLine(lltInfo, rsTestInfoPListCFBundleIdentifierUpdatedFromLpi);
            f.CFBundleIdentifier := lpi.UniformTypeIdentifier;
            f.SaveToFile;
          end;
        end;
      finally
        lpi.Free;
      end;
    end;

    if Length(f.LSMinimumSystemVersion) = 0 then begin
      f.LSMinimumSystemVersion := '10.12.6';
      LogLine(lltInfo, Format('Updated missing LSMinimumSystemVersion to %s', [f.LSMinimumSystemVersion]));
      f.SaveToFile;
    end;
    if Length(f.LSApplicationCategoryType) = 0 then begin
      f.LSApplicationCategoryType := 'public.app-category.productivity';
      LogLine(lltInfo, Format('Updated missing LSApplicationCategoryType to %s', [f.LSApplicationCategoryType]));
      f.SaveToFile;
    end;

    TestFieldSet('CFBundleName', f.CFBundleName);
    TestFieldSet('CFBundleDisplayName', f.CFBundleDisplayName);
    TestFieldSet('CFBundleIdentifier', f.CFBundleIdentifier);
    TestFieldSet('CFBundleShortVersionString', f.CFBundleShortVersionString);
    TestFieldSet('CFBundleIconFile', f.CFBundleIconFile);
    TestFieldSet('LSApplicationCategoryType', f.LSApplicationCategoryType);
    TestFieldSet('LSMinimumSystemVersion', f.LSMinimumSystemVersion);
    if f.CFBundleSupportedPlatforms.IndexOf('MacOSX') >= 0 then begin
      LogLine(lltSuccess, 'Info.plist CFBundleSupportedPlatforms lists MacOSX.');
    end else begin
      LogLine(lltError, 'Info.plist CFBundleSupportedPlatforms MacOSX entry missing!');
      Exit;
    end;

  finally
    f.Free;
  end;
  Result := True;
end;

function TLazarusMacAppWizard.TestGateKeeperAppBundle: boolean;
begin
  FLastActionLog.Clear;
  if mabwoAppStore in FOptions then begin
    LogLine(lltInfo, 'Skipping GateKeeper application bundle test for app store');
    Result := true;
    Exit;
  end;
  LogLine(lltHeader, rsTestGateKeeperHeader);
  LogLine(lltInfo, Format(rsInfoAppBundle, [FAppBundlePath]), 2);
  // this is compatible with "Developer ID Application" keys.
  // this is not compatible with "3rd Party Developer Application" keys.
  Result := ExecuteProcess('spctl', '--assess --type execute -vv "' + FAppBundlePath + '"', [0]);
  if Result then begin
    LogLine(lltSuccess, rsTestGateKeeperSuccess);
  end else begin
    LogLine(lltError, rsTestGateKeeperFailure);
  end;
end;

function TLazarusMacAppWizard.TestGateKeeperInstaller: boolean;
begin
  FLastActionLog.Clear;
  if mabwoAppStore in FOptions then begin
    LogLine(lltInfo, 'Skipping GateKeeper installer test for app store');
    Result := true;
    Exit;
  end;
  LogLine(lltHeader, rsTestGateKeeperHeader);
  LogLine(lltInfo, Format(rsInfoInstaller, [FInstallerPath]), 2);
  Result := ExecuteProcess('spctl', '--assess --type install -vv "' + FInstallerPath + '"', [0]);
  if Result then begin
    LogLine(lltSuccess, rsTestGateKeeperSuccess);
  end else begin
    LogLine(lltError, rsTestGateKeeperFailure);
  end;
end;

function TLazarusMacAppWizard.SignApplicationBundle: boolean;
var
  sTargetFilename: string;
begin
  FLastActionLog.Clear;
  LogLine(lltHeader, rsSignApplicationBundleHeader);
  LogLine(lltInfo, Format(rsInfoAppBundle, [FAppBundlePath]), 2);
  LogLine(lltInfo, Format(rsInfoEntitlements, [FEntitlementsPath]), 2);
  if mabwoAppStore in FOptions then begin
    LogLine(lltInfo, Format(rsInfoSigningIdentityAppBundle, [FSigningIdentityThirdPartyMacDevAppBundle]), 2);
  end else begin
    LogLine(lltInfo, Format(rsInfoSigningIdentityAppBundle, [FSigningIdentityDeveloperIDAppBundle]), 2);
  end;
  if mabwoNoDeepSign in FOptions then begin
    sTargetFilename := IncludeTrailingPathDelimiter(FAppBundlePath) + 'Contents/MacOS/' + ExtractFileName(FBinaryPath);
    Result := ExecuteProcess('codesign', GetAppBundleCodeSigningParameters(sTargetFilename, False), [0]);
  end;
  Result := ExecuteProcess('codesign', GetAppBundleCodeSigningParameters(FAppBundlePath, True), [0]);
  if Result then begin
    LogLine(lltSuccess, rsSignApplicationBundleSuccess);
  end else begin
    LogLine(lltError, rsSignApplicationBundleFailure);
  end;
end;

function TLazarusMacAppWizard.SignInstaller: boolean;
var
  sOld: string;
begin
  FLastActionLog.Clear;
  LogLine(lltHeader, rsSignInstallerHeader);
  LogLine(lltInfo, Format(rsInfoAppBundle, [FAppBundlePath]), 2);
  LogLine(lltInfo, Format(rsInfoSigningIdentityAppBundle, [FSigningIdentityThirdPartyMacDevAppBundle]), 2);
  LogLine(lltInfo, Format(rsInfoInstaller, [FInstallerPath]), 2);
  sOld := StringReplace(FInstallerPath, '.pkg', '-unsigned.pkg', [rfIgnoreCase]);
  DeleteFile(sOld);
  if not RenameFile(FInstallerPath, sOld) then begin
    LogLine(lltError, rsSignInstallerRenameFailure);
    Exit;
  end;
  Result := ExecuteProcess('productsign', GetInstallerCodeSigningParameters(sOld), [0]);
  if Result then begin
    ExecuteProcess('codesign', '--verify --verbose=4 "' + FInstallerPath + '"', [0]);
    LogLine(lltSuccess, rsSignInstallerSuccess);
  end else begin
    LogLine(lltError, rsSignInstallerFailure);
  end;
end;

function TLazarusMacAppWizard.NotarizeInstaller: boolean;
begin
  FLastActionLog.Clear;
  if mabwoAppStore in FOptions then begin
    LogLine(lltInfo, 'Skipping notarizing for app store.');
    Result := true;
    Exit;
  end;
  if not (mabwoNotarize in FOptions) then begin
    LogLine(lltInfo, 'Skipping notarizing due to user wish.');
    Result := true;
    Exit;
  end;
  LogLine(lltHeader, rsNotarizeInstallerHeader);
  LogLine(lltInfo, Format(rsInfoAppleID, [FAppleID]), 2);
  LogLine(lltInfo, Format(rsInfoKeyChainEntryName, [FKeychainEntryName]), 2);
  LogLine(lltInfo, Format(rsInfoInstaller, [FInstallerPath]), 2);
  LogLine(lltInfo, Format(rsInfoPackageId, [FPackageID]), 2);
  if Length(FAppleID) = 0 then begin
    LogLine(lltHeader, rsNotarizeInstallerErrorNoAppleID);
    Result := False;
    Exit;
  end;
  if Length(FKeychainEntryName) = 0 then begin
    LogLine(lltHeader, rsNotarizeInstallerErrorNoKeyChainEntryName);
    Result := False;
    Exit;
  end;
  if Length(FInstallerPath) = 0 then begin
    LogLine(lltHeader, rsNotarizeInstallerErrorNoInstallerPath);
    Result := False;
    Exit;
  end;
  Result := ExecuteProcess('xcrun', GetInstallerNotarizingParameters, [0]);
  if Result then begin
    LogLine(lltSuccess, rsNotarizeInstallerSuccess);
    // [e] RequestUUID = ....
  end else begin
    LogLine(lltError, rsNotarizeInstallerFailure);
  end;
end;

function TLazarusMacAppWizard.TestEntitlements: boolean;
begin
  FLastActionLog.Clear;
  LogLine(lltHeader, rsTestEntitlementsHeader);
  LogLine(lltInfo, Format(rsInfoEntitlements, [FEntitlementsPath]), 2);
  Result := ExecuteProcess('plutil', FEntitlementsPath, [0]);
  if Result then begin
    LogLine(lltSuccess, rsTestEntitlementsSuccess);
  end else begin
    LogLine(lltError, rsTestEntitlementsFailure);
  end;
end;

function TLazarusMacAppWizard.TestIcon: boolean;
var
  sFilenameInfo: string;
  sFilenameIcon: string;
  nsFilenameIcon: NSString;
  f: TAppleInfoPList;
  icns: NSImage;
  a: NSArray;
  i: integer;
  rep: NSImageRep;
  bHas512: boolean;
  bHas1024: boolean;
begin
  Result := False;
  FLastActionLog.Clear;
  bHas512 := False;
  bHas1024 := False;
  LogLine(lltHeader, rsTestIconHeader);
  sFilenameInfo := IncludeTrailingPathDelimiter(FAppBundlePath) + 'Contents/Info.plist';
  if not FileExists(sFilenameInfo) then begin
    Exit;
  end;
  f := TAppleInfoPList.Create;
  try
    f.LoadFromFile(sFilenameInfo);
    sFilenameIcon := IncludeTrailingPathDelimiter(FAppBundlePath) + 'Contents/Resources/' + f.CFBundleIconFile;
    if not FileExists(sFilenameIcon) then begin
      LogLine(lltError, Format(rsTestIconFileMissing, [sFilenameIcon]));
      Exit;
    end;
    LogLine(lltInfo, Format(rsTestIconInfoIconFilename, [sFilenameIcon]), 3);
    nsFilenameIcon := NSStr(PChar(sFilenameIcon));
    try
      icns := NSImage.alloc.initWithContentsOfFile(nsFilenameIcon);
      try
        // https://developer.apple.com/documentation/appkit/nsimage/1519858-representations?language=objc
        LogLine(lltInfo, Format(rsTestIconInfoImageRepresentationCount, [icns.representations.Count]));
        a := icns.representations;
        for i := 0 to Pred(a.Count) do begin
          rep := NSImageRep(a.objectAtIndex(i));
          LogLine(lltInfo, Format(rsTestIconInfoImageRepresentation, [i, rep.pixelsWide, rep.pixelsHigh, rep.bitsPerSample]), 3);
          if (rep.pixelsWide = 1024) then begin
            bHas1024 := True;
          end;
          if (rep.pixelsWide = 512) then begin
            bHas512 := True;
          end;
        end;
        Result := (bHas512 and bHas1024);
        if Result then begin
          LogLine(lltSuccess, rsTestIconIconsMissing);
        end else begin
          if not bHas512 then begin
            LogLine(lltError, rsTestIconErrorMissing512);
          end;
          if not bHas1024 then begin
            LogLine(lltError, rsTestIconErrorMissing1024);
          end;
        end;
      finally
        icns.Release;
      end;
    except
      on E: Exception do begin
        LogLine(lltError, Format(rsTestIconException, [E.Message]));
      end;
    end;
  finally
    f.Free;
  end;
end;

function TLazarusMacAppWizard.ForgetPackageId: boolean;
begin
  FLastActionLog.Clear;
  LogLine(lltHeader, rsForgetPackageIdHeader);
  LogLine(lltInfo, Format(rsInfoPackageId, [FPackageID]), 2);
  Result := ExecuteProcess('pkgutil', '--forget ' + FPackageID, [0, 1]);
  if Result then begin
    LogLine(lltSuccess, rsForgetPackageIdSuccess);
  end else begin
    LogLine(lltError, rsForgetPackageIdFailure);
  end;
end;

function TLazarusMacAppWizard.ChangePermissions: boolean;

  function ChangePermissionsStacked(APath: string): boolean;
  var
    i: integer;
    sr: TSearchRec;
    slStack: TStringList;
    sPath: string;
  begin
    Result := True;
    slStack := TStringList.Create;
    try
      slStack.Add(APath);
      while slStack.Count > 0 do begin
        sPath := IncludeTrailingPathDelimiter(slStack[0]);
        slStack.Delete(0);
        LogLine(lltInfo, Format(rsBrowsePathLevel, [sPath]), 4);
        i := FindFirst(sPath + '*', faAnyFile, sr);
        if i = 0 then begin
          try
            repeat
              LogLine(lltInfo, Format(rsBrowseTesting, [Trim(sr.Name)]), 4);
                {$IFDEF Darwin}
              if fpChmod(sPath + sr.Name, &755) <> 0 then begin
                LogLine(lltInfo, Format(rsChangePermissionsFileFailure, [sPath + sr.Name]));
                Result := False;
              end;
                {$ENDIF Darwin}
              if (sr.Attr and faDirectory) = faDirectory then begin
                if (sr.Name <> '.') and (sr.Name <> '..') then begin
                  LogLine(lltInfo, Format(rsBrowseAddingToStack, [sr.Name]), 4);
                  slStack.Add(sPath + sr.Name);
                end;
              end;
              i := FindNext(sr);
            until i <> 0;
          finally
            FindClose(sr);
          end;
        end;
      end;
    finally
      slStack.Free;
    end;
  end;

begin
  FLastActionLog.Clear;
  LogLine(lltHeader, rsChangePermissionsHeader);
  LogLine(lltInfo, Format(rsInfoAppBundle, [FAppBundlePath]), 2);
  Result := ChangePermissionsStacked(FAppBundlePath);
  if Result then begin
    LogLine(lltSuccess, rsChangePermissionsSuccess);
  end else begin
    LogLine(lltError, rsChangePermissionsFailure);
  end;
end;

function TLazarusMacAppWizard.InfoPListBinaryToXML: boolean;
begin
  FLastActionLog.Clear;
  LogLine(lltHeader, rsInfoPListBinaryToXMLHeader);
  LogLine(lltInfo, Format(rsInfoAppBundle, [FAppBundlePath]), 2);
  Result := ExecuteProcess('plutil', '-convert xml1 "' + FAppBundlePath + '/Contents/Info.plist"', [0]);
  if Result then begin
    LogLine(lltSuccess, rsInfoPListBinaryToXMLSuccess);
  end else begin
    LogLine(lltError, rsInfoPListBinaryToXMLFailure);
  end;
end;

function TLazarusMacAppWizard.RemoveExtendedAttributes: boolean;
begin
  FLastActionLog.Clear;
  LogLine(lltHeader, rsRemoveExtendedAttributesHeader);
  LogLine(lltInfo, Format(rsInfoAppBundle, [FAppBundlePath]), 2);
  Result := ExecuteProcess('xattr', '-cr "' + FAppBundlePath + '"', [0]);
  if Result then begin
    LogLine(lltSuccess, rsRemoveExtendedAttributesSuccess);
  end else begin
    LogLine(lltError, rsRemoveExtendedAttributesFailure);
  end;
end;

function TLazarusMacAppWizard.RemoveMetaFiles: boolean;
var
  iRemaining: integer;

  procedure RemoveDSStore(APath: string);
  var
    i: integer;
    sr: TSearchRec;
    slStack: TStringList;
    sPath: string;
  const
    sToRemove1 = '.DS_Store';
    sToRemove2 = 'Icon';
  begin
    slStack := TStringList.Create;
    try
      slStack.Add(APath);
      while slStack.Count > 0 do begin
        sPath := IncludeTrailingPathDelimiter(slStack[0]);
        slStack.Delete(0);
        LogLine(lltInfo, Format(rsBrowsePathLevel, [sPath]), 4);
        i := FindFirst(sPath + '*', faAnyFile, sr);
        if i = 0 then begin
          try
            repeat
              LogLine(lltInfo, Format(rsBrowseTesting, [Trim(sr.Name)]), 4);
              if (Trim(sr.Name) = sToRemove1) or (Trim(sr.Name) = sToRemove2) then begin
                if DeleteFile(sPath + sr.Name) then begin
                  LogLine(lltSuccess, Format(rsCreateInstallerRemoveSuccess, [sPath + sr.Name]));
                end else begin
                  LogLine(lltError, Format(rsCreateInstallerRemoveFailure, [sPath + sr.Name]));
                  Inc(iRemaining);
                end;
              end else if (sr.Attr and faDirectory) = faDirectory then begin
                if (sr.Name <> '.') and (sr.Name <> '..') then begin
                  LogLine(lltInfo, Format(rsBrowseAddingToStack, [sr.Name]), 4);
                  slStack.Add(sPath + sr.Name);
                end;
              end;
              i := FindNext(sr);
            until i <> 0;
          finally
            FindClose(sr);
          end;
        end;
      end;
    finally
      slStack.Free;
    end;
  end;

begin
  iRemaining := 0;
  FLastActionLog.Clear;
  LogLine(lltHeader, rsRemoveMetaFilesHeader);
  LogLine(lltInfo, Format(rsInfoAppBundle, [FAppBundlePath]), 2);
  RemoveDSStore(FAppBundlePath);
  Result := (iRemaining = 0);
  if Result then begin
    LogLine(lltSuccess, rsRemoveMetaFilesSuccess);
  end else begin
    LogLine(lltError, rsRemoveMetaFilesFailure);
  end;
end;

function TLazarusMacAppWizard.CreateInstaller: boolean;
begin
  FLastActionLog.Clear;
  LogLine(lltHeader, rsCreateInstallerHeader);
  LogLine(lltInfo, Format(rsInfoAppBundle, [FAppBundlePath]), 2);
  LogLine(lltInfo, Format(rsInfoInstaller, [FInstallerPath]), 2);
  Result := ExecuteProcess('productbuild', GetCreateInstallerParameters, [0]);
  if Result then begin
    LogLine(lltSuccess, rsCreateInstallerSuccess);
  end else begin
    LogLine(lltError, rsCreateInstallerFailure);
  end;
end;

end.
