# Purpose

This project is intended to guide the whole way from a single Darwin executable created by Lazarus, over a codesigned application bundle, up to a notarized codesigned installer that might be ready for the App Store.

# Open Tasks

* Complete Entitlements editor (currently only sandboxing is complete)
* Complete Info.plist editor
* Create entitlements if missing
* Create Info.plist if missing
* Finish notarize results viewer
* Load and store information from/in .lpi file

# Current Steps

* Tests Application Bundle (required Info.plist properties).
* Updates Info.plist from Lazarus project file (product name, product version, package id, ...).
* Tells the system to forget the current package location.
* Tests if entitlements file is valid.
* Updates files permission to allow group access.
* Removes Extended Attributes from app bundle.
* Removes meta files (.DS_Store, Icon).
* Tests if all required icons are in bundle.
* Copies the binary into the bundle.
* Codesignes the application bundle.
* Creates an installer package.
* Signs the installer package.
* Lets Apple notarize the installer package.

# Screenshots

## Developer Settings

![Developer Settings](screenshots/settings-developer.png)

## Notarize Results

![Notarize Results](screenshots/notarize-results.png)
